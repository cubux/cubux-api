Тикеты
======

Адрес: `/api/v1/feedback`


Создание тикета
---------------

### Запрос

`POST /api/v1/feedback`

### Ответ

Результат есть созданый объект [`Cubux.FeedbackTicket`][Cubux.FeedbackTicket].

### Пример

Запрос:

    POST /api/v1/feedback HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 97

    {
        "message": "Lorem ipsum dolor.\nSit amet concestepture."
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "id":         37,
        "created_at": "2017-01-31T12:34:56Z",
        "message":    "Lorem ipsum dolor.\nSit amet concestepture."
    }


[Cubux.FeedbackTicket]: ../type/feedback-ticket.md
