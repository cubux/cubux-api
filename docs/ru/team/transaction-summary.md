Итоги по транзакциям
====================

Адрес: `/api/v1/transaction-summary/team/<ID-команды>`

Получение итогов
----------------

### Запрос

`GET /api/v1/transaction-summary/team/<ID-команды>`

Параметры аналогичны [запросу истории транзакций][transactions].

### Ответ

Поле             | Тип                           | Описание
---------------- | ----------------------------- | --------
`total_income`   | дробное                       | Итог дохода в основной валюте
`total_expense`  | дробное                       | Итог расхода в основной валюте
`total_incomes`  | `Array<Cubux.CurrencyAmount>` | Детализация итога дохода по валютам
`total_expenses` | `Array<Cubux.CurrencyAmount>` | Детализация итога расхода по валютам

#### Тип `Cubux.CurrencyAmount`

Объект со следующими полями:

Поле            | Тип     | Описание
--------------- | ------- | --------
`currency_code` | строка  | Код валюты
`amount`        | дробное | Сумма


[transactions]: transaction.md
