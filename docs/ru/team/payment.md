Приход и Расход
===============

Адрес:

*   Расход: `/api/v1/payment-expense/team/<ID-команды>`
*   Приход: `/api/v1/payment-income/team/<ID-команды>`

### Тип `Cubux.Payment`

Объект со следующими полями:

Поле            | Тип             | По умолчанию    | Описание
--------------- | --------------- | --------------- | ---------------------
`id`            | целое           | —               | ID операции
`amount`        | строка          | **Обязательно** | Сумма списания
`currency_code` | строка          | **Обязательно** | Код валюты списания
`account`       | целое           | **Обязательно** | ID счета списания (валюта должна совпадать)
`category`      | целое           | **Обязательно** | ID категории (см. [категории][]). Тип категории должен совпадать с типом операции.
`date`          | дата `D.M.YYYY` | сегодня         | Дата операции
`description`   | строка          | _(нет)_         | Описание


Создание операции
-----------------

### Запрос

*   Расход: `POST /api/v1/payment-expense/team/<ID-команды>`
*   Приход: `POST /api/v1/payment-income/team/<ID-команды>`

В теле запроса передаётся объект `Cubux.Payment`.

### Ответ

Результат есть созданый объект [`Cubux.TransactionInfo`][Cubux.TransactionInfo].

### Пример

Запрос:

    POST /api/v1/payment-expense/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 36

    {
        "amount": 123.45,
        "currency_code": "RUB",
        "account": 10,
        "category": 213
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 246
    Content-Type: application/json; charset=UTF-8

    {
        "id":             338,
        "date":           "2016-04-25",
        "category_id":    213,
        "category":       "Sit amet",
        "user_name":      "Foo bar",
        "description":    "",
        "type":           "minus",
        "amount":         "123.45",
        "amount2":        null,
        "currency_code":  "RUB",
        "currency_code2": null,
        "account":        1,
        "account2":       null,
        "canUpdate":      true
    }


Изменение операции
------------------

### Запрос

*   Расход: `PATCH /api/v1/payment-expense/team/<ID-команды>/<ID-операции>`
*   Приход: `PATCH /api/v1/payment-income/team/<ID-команды>/<ID-операции>`

В теле запроса передаётся объект с полями `Cubux.Payment`.

### Ответ

Результат есть обновленный объект [`Cubux.TransactionInfo`][Cubux.TransactionInfo].

### Пример

Запрос:

    PATCH /api/v1/payment-expense/team/37/338 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 14

    {
        "amount": 200
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id":             338,
        "date":           "2016-04-25",
        "category_id":    213,
        "category":       "Sit amet",
        "user_name":      "Foo bar",
        "description":    "",
        "type":           "minus",
        "amount":         "200",
        "amount2":        null,
        "currency_code":  "RUB",
        "currency_code2": null,
        "account":        1,
        "account2":       null,
        "canUpdate":      true
    }


[категории]: category.md
[Cubux.TransactionInfo]: ../type/transaction-info.md
