Участники команды
=================

**ВАЖНО!** Данная группа запросов считается устаревшей, т.к. работает только с
зарегистрированными пользователями и только в режиме чтения. На замену
существует [более новое API][api.online.participant], дающее более полный
контроль.

Адрес: `/api/v1/teammate/team/<ID-команды>`


Список участников
-----------------

### Запрос

`GET /api/v1/teammate/team/<ID-команды>`

### Ответ

Поле    | Тип
------- | -----------------------------------------
`items` | `Array<Cubux.User>` (см [`Cubux.User`][Cubux.User])

### Пример

Запрос:

    GET /api/v1/teammate/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 80
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":       41,
                "username": "John Random"
            },
            {
                "id":       47,
                "username": "Jack Smith"
            }
        ]
    }


Информация о конкретном участнике
---------------------------------

### Запрос

`GET /api/v1/teammate/team/<ID-команды>/<ID-пользователя>`

### Ответ

Результат есть [`Cubux.User`][Cubux.User].

### Пример

Запрос:

    GET /api/v1/teammate/team/37/41 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 34
    Content-Type: application/json; charset=UTF-8

    {
        "id":       41,
        "username": "John Random"
    }


[Cubux.User]: ../type/user.md
[api.online.participant]: participant.md
