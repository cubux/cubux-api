Уведомление о завершении подписки
=================================

Основной адрес: `/api/v1/subscription-notify`


Состояние уведомления
---------------------

### Запрос

`GET /api/v1/subscription-notify/team/<ID-команды>`

### Ответ

Результат есть объект [`Cubux.SubscribeNotify`][Cubux.SubscribeNotify].

### Пример 1

Подписка активна, уведомление за 30 дней - отклонено пользователем ("больше не показывать").

Запрос:

    GET /api/v1/subscription-notify/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 60
    Content-Type: application/json; charset=UTF-8

    {
        "stage":     30,
        "discarded": true,
        "until":     "2017-08-14T07:51:57Z"
    }


### Пример 2

Подписка была и закончилась (перешла в бесплатную). Уведомление ещё не отклонено пользователем.

Запрос:

    GET /api/v1/subscription-notify/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "stage":     0,
        "discarded": true,
        "until":     null
    }


### Пример 3

Подписки нет (т.е. бесплатная подписка по умолчанию).

Запрос:

    GET /api/v1/subscription-notify/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "stage":     null,
        "discarded": null,
        "until":     null
    }


Отклонение уведомления
----------------------

### Запрос

`PATCH /api/v1/subscription-notify/team/<ID-команды>`

В теле запроса передаётся объект со следующими полями:

Поле    | Тип   | Описание
------- | ----- | --------
`stage` | целое | Этап уведомления, которое пользователь отклоняет


### Ответ

Результат есть объект [`Cubux.SubscribeNotify`][Cubux.SubscribeNotify].



[Cubux.SubscribeNotify]: ../type/subscribe-notify.md
