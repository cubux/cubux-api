Плановые операции
=================

Адрес: `/api/v1/plan/team/<ID-команды>`


Список плановых операций
------------------------

### Запрос

`GET /api/v1/plan/team/<ID-команды>`

Поддерживается [Деление на страницы][misc.pagination].

Параметры:

Параметр | Тип                       | По умолчанию | Описание
-------- | ------------------------- | ------------ | ---------------------
`type`   | enum: `expense`, `income` | _(нет)_      | Ограничение по направлению

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Plan>` (см. [`Cubux.Plan`][Cubux.Plan])

### Пример

Запрос:

    GET /api/v1/plan/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 885
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":            17,
                "type":          "expense",
                "category_id":   67,
                "account_id":    1,
                "amount":        "55.00",
                "description":   "",
                "currency_code": "RUB",
                "when":          ["10", "25"]
            },
            {
                "id":            18,
                "type":          "income",
                "category_id":   50,
                "account_id":    1,
                "amount":        "25.00",
                "description":   "Lorem ipsum dolor",
                "currency_code": "RUB",
                "when":          ["d"]
            },
            {
                "id":            24,
                "type":          "expense",
                "category_id":   67,
                "account_id":    1,
                "amount":        "3.60",
                "description":   "Sit amet consectepture",
                "currency_code": "RUB",
                "when": [
                    "WD2",
                    "WD7",
                    "WD5",
                    "10",
                    "15",
                    "M3-15",
                    "M5-9",
                    "QM1-3"
                ]
            },
            ...
        ],
        "_meta": {
            "totalCount":  5,
            "pageCount":   1,
            "currentPage": 1,
            "perPage":     20
        }
    }


Информация о плановой операции
------------------------------

### Запрос

`GET /api/v1/plan/team/<ID-команды>/<ID-операции>`

### Ответ

Результат есть [`Cubux.Plan`][Cubux.Plan].

### Пример

Запрос:

    GET /api/v1/plan/team/37/24 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 195
    Content-Type: application/json; charset=UTF-8

    {
        "id":            24,
        "type":          "expense",
        "category_id":   67,
        "account_id":    1,
        "amount":        "3.60",
        "description":   "Sit amet consectepture",
        "currency_code": "RUB",
        "when": [
            "WD2",
            "WD7",
            "WD5",
            "10",
            "15",
            "M3-15",
            "M5-9",
            "QM1-3"
        ]
    }


Создание плановой операции
--------------------------

### Запрос

`POST /api/v1/plan/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.Plan`][Cubux.Plan].

### Ответ

Результат есть созданый [`Cubux.Plan`][Cubux.Plan].

### Пример

Запрос:

    POST /api/v1/plan/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 123

    {
        "type":          "expense",
        "category_id":   67,
        "account_id":    1,
        "amount":        55.00,
        "description":   "",
        "currency_code": "RUB",
        "when":          ["10", "25"]
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 133
    Content-Type: application/json; charset=UTF-8

    {
        "id":            17,
        "type":          "expense",
        "category_id":   67,
        "account_id":    1,
        "amount":        "55.00",
        "description":   "",
        "currency_code": "RUB",
        "when":          ["10", "25"]
    }


Изменение плановой операции
---------------------------

### Запрос

`PATCH /api/v1/plan/team/<ID-команды>/<ID-операции>`

В теле запроса передаются поля объекта [`Cubux.Plan`][Cubux.Plan], значения которых
нужно изменить.

### Ответ

Результат есть обновлённый объект [`Cubux.Plan`][Cubux.Plan].

### Пример

Запрос:

    PATCH /api/v1/plan/team/37/17 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 72

    {
        "amount":      50.00,
        "description": "new description",
        "when":        ["10", "25", "13"]
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 153
    Content-Type: application/json; charset=UTF-8

    {
        "id":            17,
        "type":          "expense",
        "category_id":   67,
        "account_id":    1,
        "amount":        "50.00",
        "description":   "new description",
        "currency_code": "RUB",
        "when":          ["10", "13", "25"]
    }


Удаление плановой операции
--------------------------

### Запрос

`DELETE /api/v1/plan/team/<ID-команды>/<ID-операции>`

### Пример

Запрос:

    DELETE /api/v1/plan/team/37/17 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0
    Content-Type: application/json; charset=UTF-8


[Cubux.Plan]: ../type/plan.md
[misc.pagination]: ../misc/pagination.md
