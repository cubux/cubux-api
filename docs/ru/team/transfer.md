Перевод
=======

Адрес: `/api/v1/transfer/team/<ID-команды>`

### Тип `Cubux.Transfer`

Объект со следующими полями:

Поле            | Тип             | По умолчанию                    | Описание
--------------- | --------------- | ------------------------------- | -------------------------------------------
`id`            | целое           | —                               | ID операции
`amount`        | строка          | **Обязательно**                 | Сумма списания
`currency_code` | строка          | **Обязательно**                 | Код валюты списания
`from_account`  | целое           | **Обязательно**                 | ID счета списания (валюта должна совпадать)
`to_account`    | целое           | **Обязательно**                 | ID счета зачисления
`date`          | дата `D.M.YYYY` | сегодня                         | Дата операции
`rate`          | строка          | известный курс на указаную дату | Курс (`amount * rate = сумма_зачисления`)
`description`   | строка          | _(нет)_                         | Описание

Если валюты счетов совпадают, то поле `rate` не играет роли.

**Замечание**: Если не указать курс и указать дату в будущем, то будет
использован курс, актуальный на данный момент времени. В будущем в указаную
дату реальный курс наверняка будет другим.


Информация о переводе
---------------------

### Запрос

`GET /api/v1/transfer/team/<ID-команды>/<ID-транзакции>`

Параметры:

Параметр    | Тип     | По умолчанию | Описание
----------- | ------- | ------------ | ---------------------------------------------
`forUpdate` | boolean | 0            | Необходимость наличия привилегий на изменение

При проверке прав (`forUpdate` есть `1`) в случае отсутствия привилегий
будет дан ответ `403`.

### Ответ

Результат есть `Cubux.Transfer`.

### Пример

Запрос:

    GET /api/v1/transfer/team/37/217?forUpdate=1 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 128
    Content-Type: application/json; charset=UTF-8

    {
        "id":            261,
        "from_account":  3,
        "to_account":    24,
        "rate":          "",
        "currency_code": "EUR",
        "amount":        "0.02",
        "date":          "16.03.2016",
        "description":   ""
    }


Создание перевода
-----------------

### Запрос

`POST /api/v1/transfer/team/<ID-команды>`

В теле запроса передаётся объект `Cubux.Transfer`.

### Ответ

Результат есть созданый объект `Cubux.Transfer`.

### Пример

Запрос:

    POST /api/v1/transfer/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 125

    {
        "amount":        "10",
        "currency_code": "USD",
        "from_account":  9,
        "to_account":    1,
        "rate":          66.0452,
        "date":          "25.04.2016",
        "description":   ""
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 127
    Content-Type: application/json; charset=UTF-8

    {
        "id":            42,
        "from_account":  9,
        "to_account":    1,
        "rate":          66.0452,
        "currency_code": "USD",
        "amount":        10,
        "date":          "25.04.2016",
        "description":   ""
    }


Изменение перевода
------------------

### Запрос

`PATCH /api/v1/transfer/team/<ID-команды>/<ID-транзакции>`

В теле запроса передаётся объект с полями `Cubux.Transfer`.

### Ответ

Результат есть обновленный объект `Cubux.Transfer`.

### Пример

Запрос:

    PATCH /api/v1/transfer/team/37/217 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 14

    {
        "rate": 66.07
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 135
    Content-Type: application/json; charset=UTF-8

    {
        "id":            339
        "from_account":  9,
        "to_account":    1,
        "rate":          "66.0700",
        "currency_code": "USD",
        "amount":        "10.00",
        "date":          "25.04.2016",
        "description":   ""
    }
