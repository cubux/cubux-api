Подписка
========

Основной адрес: `/api/v1/subscription`


Состояние подписки
------------------

### Запрос

`GET /api/v1/subscription/status/team/<ID-команды>`

### Ответ

Результат есть объект [`Cubux.SubscribeStatus`][Cubux.SubscribeStatus].

### Пример 1 - Премиум подписка

Запрос:

    GET /api/v1/subscription/status/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "type": "premium",
        "deadline": "2017-08-31T23:30:00Z",
        "features": [
            {
                "name": "bank_sync",
                "active": true
            }
        ],
        "until": "2017-08-14T07:51:57Z"
    }

### Пример 2 - бесплатная подписка

Запрос:

    GET /api/v1/subscription/status/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "type": null,
        "deadline": null,
        "features": [
            {
                "name": "bank_sync",
                "active": false
            }
        ],
        "until": null
    }



Цены
----

### Запрос

`GET /api/v1/subscription/price/<код-страны>`

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.SubscribePlan>` (см. [`Cubux.SubscribePlan`][Cubux.SubscribePlan])

### Пример

Запрос:

    GET /api/v1/subscription/price/RUS HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "type": "premium",
                "features": [
                    {
                        "name": "bank_sync",
                        "active": false
                    }
                ],
                "prices": [
                    {
                        "name":          "month",
                        "currency_code": "RUB",
                        "price":         150
                    }
                    {
                        "name":          "year",
                        "currency_code": "RUB",
                        "price":         1500
                    },
                ]
            }
        ]
    }


[Cubux.SubscribePlan]: ../type/subscribe-plan.md
[Cubux.SubscribeStatus]: ../type/subscribe-status.md
