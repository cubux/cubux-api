Состояния счетов
================

Адрес: `/api/v1/account-status/team/<ID-команды>`


Список состояний
----------------

### Запрос

`GET /api/v1/account-status/team/<ID-команды>`

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.AccountStatus>` (см. [`Cubux.AccountStatus`][Cubux.AccountStatus])

### Пример

Запрос:

    GET /api/v1/account-status/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 124
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "account_id":    10,
                "amount":        3503.74,
                "currency_code": "RUB"
            },
            {
                "account_id":    13,
                "amount":        -2.53,
                "currency_code": "USD"
            }
        ]
    }


Состояние конкретного счета
---------------------------

### Запрос

`GET /api/v1/account-status/team/<ID-команды>/<ID-счёта>`

### Ответ

Результат есть [`Cubux.AccountStatus`][Cubux.AccountStatus].

### Пример

Запрос:

    GET /api/v1/account-status/team/37/10 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 56
    Content-Type: application/json; charset=UTF-8

    {
        "account_id":    10,
        "amount":        3503.74,
        "currency_code": "RUB"
    }


[Cubux.AccountStatus]: ../type/account-status.md
