Запланированые транзакции
=========================

Адрес: `/api/v1/plan-operation/team/<ID-команды>`

### Тип `Cubux.PlannedOperation`

Объект со следующими полями:

Поле            | Тип                       | Описание
--------------- | ------------------------- | -------------------
`type`          | enum: `expense`, `income` | Направление операции
`title`         | строка                    | Заголовок
`amount`        | дробное                   | Сумма
`currency_code` | строка                    | Код валюты (см. [валюты][])
`date`          | дата `YYYY-MM-DD`         | Дата


Список ближайщих операций
-------------------------

### Запрос

`GET /api/v1/plan-operation/team/<ID-команды>`

Параметры:

Параметр | Тип              | По умолчанию | Описание
-------- | ---------------- | ------------ | --------------------------------
`limit`  | целое от 1 до 10 | 3            | Максимальное количество операций

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.PlannedOperation>`

### Пример

Запрос:

    GET /api/v1/plan-operation/team/37?limit=2 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 204
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "type":          "expense",
                "title":         "Lorem",
                "amount":        "50.00",
                "currency_code": "RUB",
                "date":          "2016-04-28"
            },
            {
                "type":          "income",
                "title":         "Ipsum dolor",
                "amount":        "25.00",
                "currency_code": "RUB",
                "date":          "2016-04-25"
            }
        ]
    }


[валюты]: ../public/currency.md
