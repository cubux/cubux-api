Чеки
====

Адрес: `/api/v1/draft/team/<ID-команды>`


Список чеков
------------

### Запрос

`GET /api/v1/draft/team/<ID-команды>`

Поддерживается [Деление на страницы][misc.pagination].

Параметры:

Параметр    | Тип            | По умолчанию | Описание
----------- | -------------- | ------------ | -------------------------------
`is_passed` | enum: `0`, `1` | _(нет)_      | Ограничение по полю `is_passed`

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Draft>` (см. [`Cubux.Draft`][Cubux.Draft])

### Пример

Запрос:

    GET /api/v1/draft/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 885
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id": 1,
                "is_passed": false,
                "image": "https://file.cubux.net/uploads/team/0/000/037/draft/0/000/001/djevrbhlodiuur.jpg"
            }
        ],
            ...
        ],
        "_meta": {
            "totalCount":  5,
            "pageCount":   1,
            "currentPage": 1,
            "perPage":     20
        }
    }


Информация о чеке
-----------------

### Запрос

`GET /api/v1/draft/team/<ID-команды>/<ID-чека>`

### Ответ

Результат есть [`Cubux.Draft`][Cubux.Draft].

### Пример

Запрос:

    GET /api/v1/draft/team/37/1 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 195
    Content-Type: application/json; charset=UTF-8

    {
        "id": 1,
        "is_passed": false,
        "image": "https://file.cubux.net/uploads/team/0/000/037/draft/0/000/001/kierfvbuserito.jpg"
    }


Создание чека
-------------

### Запрос

`POST /api/v1/draft/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.Draft`][Cubux.Draft].

### Ответ

Результат есть созданый [`Cubux.Draft`][Cubux.Draft].

### Пример

Запрос:

    POST /api/v1/draft/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 123

    {
        "is_passed": false,
        "image_content": "iVBORw0KGgoAAAAN...AAElFTkSuQmCC"
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 133
    Content-Type: application/json; charset=UTF-8

    {
        "id": 1,
        "is_passed": false,
        "image": "https://file.cubux.net/uploads/team/0/000/037/draft/0/000/001/keruyvbkdrutybiy.jpg"
    }


Изменение чека
--------------

### Запрос

`PATCH /api/v1/draft/team/<ID-команды>/<ID-чека>`

В теле запроса передаются поля объекта [`Cubux.Draft`][Cubux.Draft], значения которых
нужно изменить.

### Ответ

Результат есть обновлённый объект [`Cubux.Draft`][Cubux.Draft].

### Пример

Запрос:

    PATCH /api/v1/draft/team/37/1 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 72

    {
        "is_passed": true
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 153
    Content-Type: application/json; charset=UTF-8

    {
        "id": 1,
        "is_passed": false,
        "image": "https://file.cubux.net/uploads/team/0/000/037/draft/0/000/001/euybvktuibyiyurekur.jpg"
    }


Удаление чека
-------------

### Запрос

`DELETE /api/v1/draft/team/<ID-команды>/<ID-чека>`

### Пример

Запрос:

    DELETE /api/v1/draft/team/37/1 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0
    Content-Type: application/json; charset=UTF-8


[Cubux.DRaft]: ../type/draft.md
[misc.pagination]: ../misc/pagination.md
