Пользователи транзакций
=======================

Адрес: `/api/v1/transaction-user/team/<ID-команды>`


Участники истории транзакций
----------------------------

### Запрос

`GET /api/v1/transaction-user/team/<ID-команды>`

Параметры:

Параметр  | Тип               | По умолчанию | Описание
--------- | ----------------- | ------------ | ----------------------------
`since`   | дата `YYYY-MM-DD` | _(нет)_      | Не ранее указаной даты, включая
`until`   | дата `YYYY-MM-DD` | _(нет)_      | Не позднее указаной даты, **не включая**


### Ответ

Поле    | Тип
------- | -----------------------------------------
`items` | `Array<Cubux.User>` (см [`Cubux.User`][Cubux.User])

Запрос:

    GET /api/v1/transaction-user/team/37?since=2016-04-01&until=2016-05-01 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 80
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":       41,
                "username": "John Random"
            },
            {
                "id":       47,
                "username": "Jack Smith"
            }
        ]
    }


[Cubux.User]: ../type/user.md
