Категории
=========

Адрес: `/api/v1/category/team/<ID-команды>`


Список категорий
----------------

### Запрос

`GET /api/v1/category/team/<ID-команды>`

Поддерживается [Деление на страницы][misc.pagination].

Параметры:

Параметр   | Тип                        | По умолчанию | Описание
---------- | -------------------------- | ------------ | ------------------
`type`     | enum: `expense`, `income`  | _(нет)_      | Фильтрация по типу операций
`parent`   | целое                      | 0            | Выбор родительской категории. По умолчанию отдаются корневые категории

### Ответ

Поле    | Тип
------- | -----------------------
`items` | `Array<Cubux.Category>` (см. [`Cubux.Category`][Cubux.Category])

### Пример

Запрос:

    GET /api/v1/category/team/37?type=expense HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 273
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":          10,
                "parent":      null,
                "type":        "expense",
                "name":        "Food",
                "color_rgb":   "#1122CC"
            },
            {
                "id":          213,
                "parent":      null,
                "type":        "expense",
                "name":        "Sit amet",
                "color_rgb":   null
            }
        ],
        "_meta": {
            "totalCount":  25,
            "pageCount":   2,
            "currentPage": 1,
            "perPage":     20
        }
    }


Информация о категории
----------------------

### Запрос

`GET /api/v1/category/team/<ID-команды>/<ID-категории>`

### Ответ

Результат есть [`Cubux.Category`][Cubux.Category].

### Пример

Запрос:

    GET /api/v1/category/team/37/213 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 96
    Content-Type: application/json; charset=UTF-8

    {
        "id":          213,
        "parent":      null,
        "type":        "expense",
        "name":        "Sit amet",
        "color_rgb":   null
    }


Создание категории
------------------

### Запрос

`POST /api/v1/category/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.Category`][Cubux.Category].

### Ответ

Результат есть созданая категория [`Cubux.Category`][Cubux.Category].

### Пример

Запрос:

    POST /api/v1/category/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 58

    {
        "type": "expense",
        "name": "Sit amet",
        "color_rgb": "#1122CC"
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 79
    Content-Type: application/json; charset=UTF-8

    {
        "id":          213,
        "parent":      null,
        "type":        "expense",
        "name":        "Sit amet",
        "color_rgb":   "#1122CC"
    }


Изменение категории
-------------------

### Запрос

`PATCH /api/v1/category/team/<ID-команды>/<ID-категории>`

В теле запроса передаётся объект [`Cubux.Category`][Cubux.Category].
Изменение следующих полей **невозможно**:

*   `type`
*   `parent`

У служебных категорий (`is_helper` = `true`) невозможно изменить поле `system_code`.

### Ответ

Результат есть обновлённый объект [`Cubux.Category`][Cubux.Category].

### Пример

Запрос:

    PATCH /api/v1/category/team/37/213 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 18

    {
        "name": "Eiusmod"
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 96
    Content-Type: application/json; charset=UTF-8

    {
        "id":          213,
        "parent":      null,
        "type":        "expense",
        "name":        "Eiusmod",
        "color_rgb":   null
    }


Удаление категории
------------------

### Запрос

`DELETE /api/v1/category/team/<ID-команды>/<ID-категории>`

**Замечание:** Категория не может быть удалена, если она используется в транзакциях.

**Замечание:** Служебные категории (`is_helper` = `true`) удалить невозможно.

### Пример

Запрос:

    DELETE /api/v1/category/team/37/213 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0


[Cubux.Category]: ../type/category.md
[misc.pagination]: ../misc/pagination.md
