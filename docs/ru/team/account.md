Счета
=====

Адрес: `/api/v1/account/team/<ID-команды>`


Список счетов
-------------

### Запрос

`GET /api/v1/account/team/<ID-команды>`

Параметры:

Параметр   | Тип                        | По умолчанию | Описание
---------- | -------------------------- | ------------ | --------------------------------
`access`   | enum: `read`, `edit`       | `read`       | Ограничение по уровню привилегий
`currency` | строка (код валюты по ISO) | _(нет)_      | Ограничение по валюте

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Account>` (см. [`Cubux.Account`][Cubux.Account])

### Пример

Запрос:

    GET /api/v1/account/team/37?access=edit HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 185
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":            10,
                "name":          "Lorem ipsum",
                "country_code":  "RUS",
                "currency_code": "RUB",
                "bank_id":       997
            },
            {
                "id":            13,
                "name":          "Sit amet",
                "country_code":  "RUS",
                "currency_code": "USD",
                "bank_id":       null
            }
        ]
    }


Информация о счёте
------------------

### Запрос

`GET /api/v1/account/team/<ID-команды>/<ID-счёта>`

### Ответ

Результат есть [`Cubux.Account`][Cubux.Account].

### Пример

Запрос:

    GET /api/v1/account/team/37/10 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 87
    Content-Type: application/json; charset=UTF-8

    {
        "id":            10,
        "name":          "Lorem ipsum",
        "country_code":  "RUS",
        "currency_code": "RUB",
        "bank_id":       997
    }


Создание счёта
--------------

### Запрос

`POST /api/v1/account/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.Account`][Cubux.Account].

### Ответ

Результат есть созданый [`Cubux.Account`][Cubux.Account].

### Пример

Запрос:

    POST /api/v1/account/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 79

    {
        "name":          "Lorem ipsum",
        "currency_code": "RUB",
        "country_code":  "RUS",
        "bank_id":       997
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 87
    Content-Type: application/json; charset=UTF-8

    {
        "id":            10,
        "name":          "Lorem ipsum",
        "country_code":  "RUS",
        "currency_code": "RUB",
        "bank_id":       997
    }


Изменение счёта
---------------

### Запрос

`PATCH /api/v1/account/team/<ID-команды>/<ID-счёта>`

В теле запроса передаются поля объекта [`Cubux.Account`][Cubux.Account], значения
которых нужно изменить.

### Ответ

Результат есть обновлённый объект [`Cubux.Account`][Cubux.Account].

### Пример

Запрос:

    PATCH /api/v1/account/team/37/10 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 79

    {
        "name":    "Consectepture"
        "bank_id": null
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 90
    Content-Type: application/json; charset=UTF-8

    {
        "id":            10,
        "name":          "Consectepture",
        "country_code":  "RUS",
        "currency_code": "RUB",
        "bank_id":       null
    }


Удаление счёта
--------------

### Запрос

`DELETE /api/v1/account/team/<ID-команды>/<ID-счёта>`

**Замечание:** Удаление счета влечет за собой удаление всех транзакций, плановых транзакций и целей,
которые связаны с этим счётом. Клиенту следует заранее предупредить об этом Пользователя.

### Пример

Запрос:

    DELETE /api/v1/account/team/37/10 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0


[Cubux.Account]: ../type/account.md
