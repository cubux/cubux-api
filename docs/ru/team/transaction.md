Транзакции
==========

Адрес: `/api/v1/transaction/team/<ID-команды>`


История транзакций
------------------

### Запрос

`GET /api/v1/transaction/team/<ID-команды>`

Поддерживается [Деление на страницы][misc.pagination].

Базовые параметры:

Параметр  | Тип                         | По умолчанию | Описание
--------- | --------------------------- | ------------ | ------------------
`type`    | enum: `incoming`, `leaving`, `transfer` | _(нет)_      | Фильтр по направлению
`category`| целое                       | _(нет)_      | ID категорий, включая дочерние [категории][]. Можно указывать несколько категорий через запятую.
`year`    | целое                       | _(нет)_      | Только за год
`month`   | целое                       | _(нет)_      | Только за месяц (вместе с годом)
`day`     | целое                       | _(нет)_      | Только за день (вместе с месяцем и годом)

Следующие наборы параметров поиска взаимно исключают друг друга:

*   Простой поиск

    Параметр | Тип    | По умолчанию | Описание
    -------- | ------ | ------------ | ------------------------------------
    `search` | строка | _(нет)_      | Поиск по названию счёта, категории, по имени пользователя и по сумме

*   Расширеный поиск

    Параметр       | Тип                              | По умолчанию | Описание
    -------------- | -------------------------------- | ------------ | ---------------
    `userId`       | целое                            | _(нет)_      | ID пользователя
    `categoryName` | строка                           | _(нет)_      | Название категории
    `accountId`    | целое                            | _(нет)_      | ID счета
    `dateSince`    | дата `D.M.YYYY` или `YYYY-MM-DD` | _(нет)_      | Не ранее указаной даты, включая
    `dateUntil`    | дата `D.M.YYYY` или `YYYY-MM-DD` | _(нет)_      | Не позднее указаной даты, включая
    `amount`       | строка                           | _(нет)_      | Сумма

### Ответ

Поле    | Тип
------- | ---
`items` | `Array<Cubux.TransactionInfo>` (см [`Cubux.TransactionInfo`][Cubux.TransactionInfo])

Запрос:

    GET /api/v1/transaction/team/37?year=2016&page=2 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4763
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":             279,
                "date":           "2016-03-25",
                "category_id":    9,
                "category":       "Food",
                "user_name":      "John Random",
                "description":    "",
                "type":           "minus",
                "amount":         "1.00",
                "amount2":        null,
                "currency_code":  "RUB",
                "currency_code2": null,
                "account":        1,
                "account2":       null,
                "canUpdate":      true
            },
            ...
        ],
        "_meta": {
            "totalCount":  60,
            "pageCount":   3,
            "currentPage": 2,
            "perPage":     20
        }
    }


Удаление транзакции
-------------------

### Запрос

`DELETE /api/v1/transaction/team/<ID-команды>/<ID-транзакции>`

### Пример

Запрос:

    DELETE /api/v1/transaction/team/37/213 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0


[валюты]: ../public/currency.md
[категории]: category.md
[счета]: account.md
[misc.pagination]: ../misc/pagination.md
[Cubux.TransactionInfo]: ../type/transaction-info.md
