Участники команды
=================

Адрес: `/api/v1/participant/team/<ID-команды>`

**Замечание:** Участник с привилегией ограниченного пользователя может лишь
запросить информацию о себе. Остальные запросы разрешены только участнику с
правами администратора команды.

**Замечание:** Поле `user_mail` объекта [`Cubux.TeamParticipant`][Cubux.TeamParticipant]
может использоваться только в запросе добавления участника. В остальных запросах
данной группы для идентификации участника используется идентификатор в поле
`user_id`.

**Замечание:** Изменить или удалить своё участие невозможно.


Список участников
-----------------

### Запрос

`GET /api/v1/participant/team/<ID-команды>`

### Ответ

Поле    | Тип
------- | ---
`items` | `Array<Cubux.TeamParticipant>` (см. [`Cubux.TeamParticipant`][Cubux.TeamParticipant])

### Пример

Запрос:

    GET /api/v1/participant/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 885
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "role_name": "admin",
                "user_id": 42,
                "user": {
                    "id": 42,
                    "username": "John Random",
                },
                "invite": null
            },
            {
                "role_name": "user",
                "user_id": 97,
                "user": null,
                "invite": {
                    "id": 97,
                    "mail": "foo-bar@example.com"
                }
            }
        ],
    }


Информация об участнике
-----------------------

### Запрос

`GET /api/v1/participant/team/<ID-команды>/<ID-участника>`

### Ответ

Результат есть [`Cubux.TeamParticipant`][Cubux.TeamParticipant].

### Пример

Запрос:

    GET /api/v1/participant/team/37/42 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 195
    Content-Type: application/json; charset=UTF-8

    {
        "role_name": "admin",
        "user_id": 42,
        "user": {
            "id": 42,
            "username": "John Random",
        },
        "invite": null
    }


Добавление участника
--------------------

### Запрос

`POST /api/v1/participant/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.TeamParticipant`][Cubux.TeamParticipant].

### Ответ

Результат есть созданый [`Cubux.TeamParticipant`][Cubux.TeamParticipant].

### Пример

Запрос:

    POST /api/v1/participant/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 123

    {
        "role_name": "admin",
        "user_mail": "lorem-ipsum@example.com"
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 133
    Content-Type: application/json; charset=UTF-8

    {
        "role_name": "admin",
        "user_id": 127,
        "user": null,
        "invite": {
            "id": 127,
            "mail": "lorem-ipsum@example.com"
        }
    }


Изменение участника
-------------------

### Запрос

`PATCH /api/v1/participant/team/<ID-команды>/<ID-участника>`

В теле запроса передаются поля объекта [`Cubux.TeamParticipant`][Cubux.TeamParticipant],
значения которых нужно изменить.

### Ответ

Результат есть обновлённый объект [`Cubux.TeamParticipant`][Cubux.TeamParticipant].

### Пример

Запрос:

    PATCH /api/v1/participant/team/37/97 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 72

    {
        "role_name": "admin"
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 153
    Content-Type: application/json; charset=UTF-8

    {
        "role_name": "admin"
        "user_id": 97,
        "user": null,
        "invite": {
            "id": 97,
            "mail": "foo-bar@example.com"
        }
    }


Удаление участника из команды
-----------------------------

### Запрос

`DELETE /api/v1/participant/team/<ID-команды>/<ID-участника>`

### Пример

Запрос:

    DELETE /api/v1/participant/team/37/97 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0
    Content-Type: application/json; charset=UTF-8


[Cubux.TeamParticipant]: ../type/team-participant.md
[misc.pagination]: ../misc/pagination.md
