Цели
====

Основной адрес: `/api/v1/target/team/<ID-команды>`


Список целей
------------

### Запрос

`GET /api/v1/target/team/<ID-команды>`

Поддерживается [Деление на страницы][misc.pagination].

Параметры:

Параметр | Тип                          | По умолчанию | Описание
-------- | ---------------------------- | ------------ | ------------------
`type`   | enum: `completed`, `pending` | _(нет)_      | Фильтрация по завершенности

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Target>` (см. [`Cubux.Target`][Cubux.Target])

### Пример

Запрос:

    GET /api/v1/target/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 4072
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":                 2,
                "name":               "Диван",
                "amount":             "100000.00",
                "currency_code":      "RUB",
                "to_account_id":      4,
                "from_account_id":    1,
                "pay_amount":         "500.00",
                "completed":          false,
                "use_reserve":        false,
                "finish_date":        null,
                "pay_interval":       "m",
                "pay_since_date":     "2015-10-26",
                "accumulated_amount": 27443.6,
                "finish_forecast":    "P12Y1M3D",
                "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
            },
            {
                "id":                 4,
                "name":               "Шкаф",
                "amount":             "7000.00",
                "currency_code":      "RUB",
                "to_account_id":      null,
                "from_account_id":    1,
                "pay_amount":         "500.00",
                "completed":          false,
                "use_reserve":        true,
                "finish_date":        null,
                "pay_interval":       null,
                "pay_since_date":     null,
                "accumulated_amount": 481,
                "finish_forecast":    null,
                "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/004/ksejuyhvbkfurt.jpg"
            },
            {
                "id":                 6,
                "name":               "Foo",
                "amount":             "10.00",
                "currency_code":      "RUB",
                "to_account_id":      null,
                "from_account_id":    null,
                "pay_amount":         "2.00",
                "completed":          true,
                "use_reserve":        true,
                "finish_date":        "2016-01-18",
                "pay_interval":       "w",
                "pay_since_date":     "2015-12-07",
                "accumulated_amount": "10.00",
                "finish_forecast":    null,
                "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/006/keuyvblotiu6roph.jpg"
            },
            ...
        ],
        "_meta": {
            "totalCount":12,
            "pageCount":1,
            "currentPage":1,
            "perPage":20
        }
    }


Информация о цели
-----------------

### Запрос

`GET /api/v1/target/team/<ID-команды>/<ID-цели>`

### Ответ

Результат есть [`Cubux.Target`][Cubux.Target].

### Пример

Запрос:

    GET /api/v1/target/team/37/2 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 361
    Content-Type: application/json; charset=UTF-8

    {
        "id":                 2,
        "name":               "Диван",
        "amount":             "100000.00",
        "currency_code":      "RUB",
        "to_account_id":      4,
        "from_account_id":    1,
        "pay_amount":         "500.00",
        "completed":          false,
        "use_reserve":        false,
        "finish_date":        null,
        "pay_interval":       "m",
        "pay_since_date":     "2015-10-26",
        "accumulated_amount": 27443.6,
        "finish_forecast":    "P12Y1M3D",
        "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
    }


Создание цели
-------------

### Запрос

`POST /api/v1/target/team/<ID-команды>`

В теле запроса передаётся объект [`Cubux.Target`][Cubux.Target].

### Ответ

Результат есть созданый [`Cubux.Target`][Cubux.Target].

### Пример

Запрос:

    POST /api/v1/target/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 292

    {
        "name":             "Диван",
        "amount":           "100000.00",
        "currency_code":    "RUB",
        "from_account_id":  1,
        "to_account_title": "Цель 'Диван'",
        "use_reserve":      false,
        "pay_amount":       "500.00",
        "pay_interval":     "m",
        "pay_since_date":   "2015-10-26",
        "image_content":    "iVBORw0KGgoAAAAN...AAElFTkSuQmCC"
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 361
    Content-Type: application/json; charset=UTF-8

    {
        "id":                 2,
        "name":               "Диван",
        "amount":             "100000.00",
        "currency_code":      "RUB",
        "to_account_id":      4,
        "from_account_id":    1,
        "pay_amount":         "500.00",
        "completed":          false,
        "use_reserve":        false,
        "finish_date":        null,
        "pay_interval":       "m",
        "pay_since_date":     "2015-10-26",
        "accumulated_amount": 27443.6,
        "finish_forecast":    "P12Y1M3D",
        "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
    }


Изменение цели
--------------

### Запрос

`PATCH /api/v1/target/team/<ID-команды>/<ID-цели>`

**Важно:** Допустимо изменение только незавершенных целей.

В теле запроса передаются поля объекта [`Cubux.Target`][Cubux.Target], значения которых
нужно изменить.

### Ответ

Результат есть обновлённый объект [`Cubux.Target`][Cubux.Target].

### Пример

Запрос:

    PATCH /api/v1/target/team/37/2 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 60

    {
        "name":            "Гарнитур",
        "amount":          "99500.00",
        "from_account_id": 1,
        "pay_amount":      "610.00",
        "pay_interval":    "m",
        "pay_since_date":  "2015-10-26",
        "image_content":   "iVBORw0KGgoAAAAN...AAElFTkSuQmCC"
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 366
    Content-Type: application/json; charset=UTF-8

    {
        "id":                 2,
        "name":               "Гарнитур",
        "amount":             "99500.00",
        "currency_code":      "RUB",
        "to_account_id":      4,
        "from_account_id":    1,
        "pay_amount":         "610.00",
        "completed":          false,
        "use_reserve":        false,
        "finish_date":        null,
        "pay_interval":       "m",
        "pay_since_date":     "2015-10-26",
        "accumulated_amount": 27443.6,
        "finish_forecast":    "P12Y1M3D",
        "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
    }


Удаление цели
-------------

### Запрос

`DELETE /api/v1/target/team/<ID-команды>/<ID-цели>`

### Пример

Запрос:

    DELETE /api/v1/target/team/37/2 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0
    Content-Type: application/json; charset=UTF-8


Завершение цели
---------------

### Запрос

`PATCH /api/v1/target-complete/team/<ID-команды>/<ID-цели>`

Цели без автоматического списания необходимо завершать вручную. Цели с
автоматическим списанием могут быть досрочно завершены вручную.

### Ответ

Результат есть обновлённый объект [`Cubux.Target`][Cubux.Target].

### Пример

Запрос:

    PATCH /api/v1/target-complete/team/37/6 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Length: 0

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 354
    Content-Type: application/json; charset=UTF-8

    {
        "id":                 6,
        "name":               "Foo",
        "amount":             "10.00",
        "currency_code":      "RUB",
        "to_account_id":      null,
        "from_account_id":    null,
        "pay_amount":         "2.00",
        "completed":          true,
        "use_reserve":        true,
        "finish_date":        "2016-01-18",
        "pay_interval":       "w",
        "pay_since_date":     "2015-12-07",
        "accumulated_amount": "10.00",
        "finish_forecast":    null,
        "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
    }


Ручное списание в пользу цели
-----------------------------

### Запрос

`PATCH /api/v1/target-pay/team/<ID-команды>/<ID-цели>`

В теле запроса передаются следующие поля:

Поле     | Тип     | По умолчанию    | Описание
-------- | ------- | --------------- | --------
`amount` | дробное | **обязательно** | Сумма

### Ответ

Результат есть обновлённый объект [`Cubux.Target`][Cubux.Target].

### Пример

Запрос:

    PATCH /api/v1/target-pay/team/37/17 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 16

    {
        "amount": 42.37
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 361
    Content-Type: application/json; charset=UTF-8

    {
        "id":                 2,
        "name":               "Диван",
        "amount":             "100000.00",
        "currency_code":      "RUB",
        "to_account_id":      4,
        "from_account_id":    1,
        "pay_amount":         "500.00",
        "completed":          false,
        "use_reserve":        false,
        "finish_date":        null,
        "pay_interval":       "m",
        "pay_since_date":     "2015-10-26",
        "accumulated_amount": 27443.6,
        "finish_forecast":    "P12Y1M3D",
        "image": "https://file.cubux.net/uploads/team/0/000/037/target/0/000/002/09n4cr48800go0skkro4o.jpg"
    }


[Cubux.Target]: ../type/target.md
[misc.pagination]: ../misc/pagination.md
