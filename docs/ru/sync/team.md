Данные команды
==============

.                 | .
----------------- | --------------------------------
Базовый адрес API | `/api/v1/sync/team/<id-команды>`
Тип данных        | [`Cubux.Sync.TeamData`][Cubux.Sync.TeamData]

**ВАЖНО:** Требуется авторизация токеном с [разрешением][scopes]:
`UserActivity`.

Данные команды с точки зрения текущего авторизованного пользователя.

**ВАЖНО:** Контекст работает только с командой, существующей на сервере.
Создание новой команды возможно из контекста [Личных данных](user.md).

Ниже описаны не совсем стандартные операции, которые поддерживаются в
данном контексте. Стандартные операции см. в [Контекстах](context.md).


Удаление команды
----------------

Для удаления команды необходимо отправить удаление записи [`Cubux.Team`][Cubux.Team]
в поле `info`:

    PATCH /api/v1/sync/team/42/203 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 34

    {
        "info": {
            "is_deleted": true
        }
    }


Добавление участника в команду
------------------------------

Добавлении участника в команду в настоящее время предусмотрено только для
существующих пользователей. При добавлении участника вместо ID пользователя
в поле `user_id` можно указывать E-mail пользователя в поле `user_mail`.
См. [`Cubux.TeamParticipant`][Cubux.TeamParticipant].


[Cubux.Team]: ../type/team.md
[Cubux.Sync.TeamData]: ../type/sync-team-data.md
[Cubux.TeamParticipant]: ../type/team-participant.md
[scopes]: ../auth/scopes.md
