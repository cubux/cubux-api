Публичные данные
================

.                 | .
----------------- | ---------------------------
Базовый адрес API | `/api/v1/sync/global`
Тип данных        | [`Cubux.Sync.GlobalData`][Cubux.Sync.GlobalData]

Публичные данные доступны только для чтения. Аутентификация не требуется.


[Cubux.Sync.GlobalData]: ../type/sync-global-data.md
