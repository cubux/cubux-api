API Синхронизации
=================

API синхронизации работает отдельно по каждому [Контексту](context.md). У
каждого типа Контекста есть свой базовый адрес для API запросов. В
описании запросов ниже этот адрес обозначен `<context-base-url>`.

Набор входные и выходные данные в разных Контекстах отличается, но имеет
общий базовый тип [`Cubux.Sync.Data`][Cubux.Sync.Data]. В описании запросов ниже вместо
конкретного типа данных используется именно этот базовый тип.


Запрос полных данных
--------------------

### Запрос

`GET <context-base-url>`

### Ответ

Результат есть [`Cubux.Sync.Data`][Cubux.Sync.Data], содержащий все актуальные данные
конекста целиком. Удаленные записи не отдаются.

### Пример

Запрос:

    GET /api/v1/sync/user HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 190
    Content-Type: application/json; charset=UTF-8

    {
        "revision": "137",
        "me": {
            "id":           42,
            "name":         "John",
            "family":       "Random",
            "country_code": "RUS",
            "username":     "John Random"
        },
        "teams": [
            {
                "team_id":   37,
                "role_name": "admin"
            },
            {
                "team_id":   91,
                "role_name": "user"
            }
        ]
    }


Запрос изменений
----------------

### Запрос

`GET <context-base-url>/<revision>`

### Ответ

Результат есть [`Cubux.Sync.Data`][Cubux.Sync.Data], содержащий только изменения, которые
были проделаны после указаной версии `<revision>`.

### Пример

Запрос:

    GET /api/v1/sync/user/131 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 105
    Content-Type: application/json; charset=UTF-8

    {
        "revision": "137",
        "me":       null,
        "teams": [
            {
                "team_id":    31,
                "is_deleted": true
            },
            {
                "team_id":   91,
                "role_name": "user"
            }
        ]
    }

В данном ответе сказано, что изменений в `me` не было, доступ к команде с
ID `31` был утрачен, а доступ к команде с ID `91` был выдан/изменён.


Отправка изменений с получением полных данных в ответе
------------------------------------------------------

### Запрос

`POST <context-base-url>`

В теле отправляется набор изменений в виде [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable].

### Ответ

Результат есть [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable], содержащий все актуальные
данные конекста целиком с учетом отправленых изменений. Удаленные записи не
отдаются.

### Пример

Запрос:

    POST /api/v1/sync/user HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 97

    {
        "me": {
            "name":     "Jack",
            "password": "new password"
        },
        "teams": [
            {
                "_local_id": "new-team-97",
                "team": {
                    "title": "New team name"
                }
            }
        ]
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "revision": "138",
        "me": {
            "id":           42,
            "name":         "Jack",
            "family":       "Random",
            "country_code": "RUS",
            "username":     "Jack Random"
        },
        "teams": [
            {
                "team_id":   37,
                "role_name": "admin"
            },
            {
                "team_id":   91,
                "role_name": "user"
            },
            {
                "team_id":   178,
                "role_name": "admin",
                "_local_id": "new-team-97"
            }
        ],
        "errors": []
    }


Отправка изменений с получением изменений
-----------------------------------------

### Запрос

`PATCH <context-base-url>/<revision>`

В теле отправляется набор изменений в виде [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable].

### Ответ

Результат есть [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable], содержащий только изменения,
которые были проделаны после указаной версии `<revision>`, включая
отправленые изменения.

### Пример

Запрос:

    PATCH /api/v1/sync/user/137 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 97

    {
        "me": {
            "name":     "Jack",
            "password": "new password"
        },
        "teams": [
            {
                "_local_id": "new-team-97",
                "team": {
                    "title": "New team name"
                }
            }
        ],
        "errors": []
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "revision": "138",
        "me": {
            "id":           42,
            "name":         "Jack",
            "family":       "Random",
            "country_code": "RUS",
            "username":     "Jack Random"
        },
        "teams": [
            {
                "team_id":   178,
                "role_name": "admin",
                "_local_id": "new-team-97"
            }
        ]
    }


[Cubux.Sync.Data]: ../type/sync-data.md
[Cubux.Sync.DataWriteable]: ../type/sync-data-writeable.md
