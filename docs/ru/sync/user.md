Личные данные
=============

.                 | .
----------------- | -------------------------
Базовый адрес API | `/api/v1/sync/user`
Тип данных        | [`Cubux.Sync.UserData`][Cubux.Sync.UserData]

**ВАЖНО:** Требуется авторизация токеном с [разрешением][scopes]:
`UserActivity`.

Персональные данные авторизованного пользователя.

Ниже описаны не совсем стандартные операции, которые поддерживаются в
данном контексте. Стандартные операции см. в [Контекстах](context.md).


Удаление аккаунта
-----------------

Для удаления пользователем своего аккаунта необходимо отправить удаление
записи [`Cubux.Me`][Cubux.Me] в поле `me`:

    PATCH /api/v1/sync/user/197 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 34

    {
        "me": {
            "is_deleted": true
        }
    }


Создание команды
----------------

Для создания новой команды необходимо отправить запись типа
[`Cubux.MyTeam`][Cubux.MyTeam] в списке `teams`, снабдив запись, как полагается,
локальным идентификатором.

Пример запроса создания двух команд:

    PATCH /api/v1/sync/user/197 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 203

    {
        "teams": [
            {
                "_local_id": "my-participation-42",
                "team": {
                    "title":                 "New team A",
                    "default_currency_code": "RUB"
                }
            },
            {
                "_local_id": "my-participation-47",
                "team": {
                    "title":                 "New team B",
                    "default_currency_code": "fail"
                }
            }
        ]
    }

Пример ответа

    HTTP/1.1 200 OK
    Content-Length: 346
    Content-Type: application/json; charset=UTF-8

    {
        "revision": 198,
        "me":       null,
        "teams": [
            {
                "role_name": "admin",
                "team_id":   64,
                "_local_id": "my-participation-42"
            }
        ],
        "errors": [
            {
                "entity":   "teams",
                "key":      null,
                "local_id": "my-participation-47",
                "errors": [
                    {
                        "field":   "default_currency_code",
                        "message": "Значение «Default Currency Code» должно содержать максимум 3 символа."
                    }
                ]
            }
        ]
    }


[Cubux.Me]: ../type/me.md
[Cubux.MyTeam]: ../type/my-team.md
[Cubux.Sync.UserData]: ../type/sync-user-data.md
[scopes]: ../auth/scopes.md
