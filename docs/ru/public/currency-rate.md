Курсы валют
=======

Адрес: `/api/v1/currency-rate`

Для конвертации _N_ единиц валюты _AAA_ в валюту _BBB_ нужно запросить курс
`AAA-BBB` и умножить _N_ на полученое отношение `coeff`:

                      BBB
    N [AAA] * coeff [ --- ] = M [BBB]
                      AAA

Информация о курсе
------------------

### Запрос

`GET /api/v1/currency-rate/<код-исходной-валюты>-<код-целевой-валюты>`

Параметры:

Параметр | Тип               | По умолчанию | Описание
-------- | ----------------- | ------------ | ----------------------------
`date`   | дата `yyyy-mm-dd` | _(сегодня)_  | Дата, для которой нужен курс

### Ответ

Результат есть объект [`Cubux.CurrencyRate`][Cubux.CurrencyRate].

### Пример

Запрос:

    GET /api/v1/currency-rate/USD-RUB?date=2016-04-26 HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 74
    Content-Type: application/json; charset=UTF-8

    {
        "from":  "USD",
        "to":    "RUB",
        "coeff": 66.0452,
        "date":  "2016-04-26"
    }


[Cubux.CurrencyRate]: ../type/currency-rate.md
