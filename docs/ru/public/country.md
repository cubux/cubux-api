Страны
=======

Адрес: `/api/v1/country`


Список стран
------------

### Запрос

`GET /api/v1/country`

### Ответ

Поле    | Тип
------- | ----------------------
`items` | `Array<Cubux.Country>` (см [`Cubux.Country`][Cubux.Country])

### Пример

Запрос:

    GET /api/v1/country HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 1698
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "code":                  "AUT",
                "code2":                 "AT",
                "default_currency_code": "EUR",
                "title":                 "Австрия",
                "flag": "/uploads/file/5f/dv/p132z84c40g84cwccow8s/austria.png"
            },
            {
                "code":                  "DEU",
                "code2":                 "DE",
                "default_currency_code": "EUR",
                "title":                 "Германия",
                "flag": null
            },
            {
                "code":                  "RUS",
                "code2":                 "RU",
                "default_currency_code": "RUB",
                "title":                 "Российская Федерация",
                "flag": "/uploads/file/cj/q1/es6hivk8k0kk8ggkckwk8/1452678932-russian-federation.png"
            },
            ...
        ]
    }


Информация о стране
-------------------

### Запрос

`GET /api/v1/country/<код-страны>`

В качестве значения `<код-страны>` необходимо импользовать трехбуквеный код
страны из атрибута `code`.

### Ответ

Результат есть объект [`Cubux.Country`][Cubux.Country].

### Пример

Запрос:

    GET /api/v1/country/RUS HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 179
    Content-Type: application/json; charset=UTF-8

    {
        "code":                  "RUS",
        "code2":                 "RU",
        "default_currency_code": "RUB",
        "title":                 "Российская Федерация",
        "flag": "/uploads/file/cj/q1/es6hivk8k0kk8ggkckwk8/1452678932-russian-federation.png"
    }


[Cubux.Country]: ../type/country.md
