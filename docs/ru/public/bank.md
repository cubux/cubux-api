Банки
=======

Адрес: `/api/v1/bank`


Список банков страны
--------------------

### Запрос

`GET /api/v1/bank/<код-страны>`

См. [страны][].

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Bank>` (см [`Cubux.Bank`][Cubux.Bank])

### Пример

Запрос:

    GET /api/v1/bank/RUS HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 409
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":           2,
                "country_code": "RUS",
                "name":         "Lorem",
                "logo": "/uploads/file/48/nc/ojpuhz284org08o80ggs4/lorem.png"
            },
            {
                "id":           3,
                "country_code": "RUS",
                "name":         "Ipsum",
                "logo": null
            },
            ...
        ]
    }


Информация о банке
------------------

### Запрос

`GET /api/v1/bank/<ID-банка>`

### Ответ

Результат есть объект [`Cubux.Bank`][Cubux.Bank].

### Пример

Запрос:

    GET /api/v1/bank/2 HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 84
    Content-Type: application/json; charset=UTF-8

    {
        "id":           2,
        "country_code": "RUS",
        "name":         "Lorem",
        "logo":         "/uploads/file/48/nc/ojpuhz284org08o80ggs4/lorem.png"
    }


[Cubux.Bank]: ../type/bank.md
[страны]: country.md
