Валюты
=======

Адрес: `/api/v1/currency`

Список валют
------------

### Запрос

`GET /api/v1/currency`

### Ответ

Поле    | Тип
------- | -----------------------
`items` | `Array<Cubux.Currency>` (см. [`Cubux.Currency`][Cubux.Currency])

### Пример

Запрос:

    GET /api/v1/currency HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 278
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "code":    "EUR",
                "prefix":  "€",
                "postfix": null,
                "symbol":  "€",
                "title":   "Евро"
            },
            {
                "code":    "RUB",
                "prefix":  null,
                "postfix": "₽",
                "symbol":  "₽",
                "title":   "Российский рубль"
            },
            {
                "code":    "USD",
                "prefix":  "$",
                "postfix": null,
                "symbol":  "$",
                "title":   "Доллар США"
            },
            ...
        ]
    }


Информация о валюте
-------------------

### Запрос

`GET /api/v1/currency/<код-валюты>`

### Ответ

Результат есть объект [`Cubux.Currency`][Cubux.Currency].

### Пример

Запрос:

    GET /api/v1/currency/RUB HTTP/1.1
    Host: www.cubux.net

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 101
    Content-Type: application/json; charset=UTF-8

    {
        "code":    "RUB",
        "prefix":  null,
        "postfix": "₽",
        "symbol":  "₽",
        "title":   "Российский рубль"
    }


[Cubux.Currency]: ../type/currency.md
