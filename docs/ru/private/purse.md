Лицевой счёт
============

Адрес: `/api/v1/purse`


Список кошельков
----------------

### Запрос

`GET /api/v1/purse`

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Purse>` (см. [`Cubux.Purse`][Cubux.Purse])

### Пример

Запрос:

    GET /api/v1/purse HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 116
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "amount":        37,
                "currency_code": "RUB"
            },
            {
                "amount":        0,
                "currency_code": "EUR"
            },
            {
                "amount":        1.23,
                "currency_code": "USD"
            }
        ]
    }


Информация о кошельке
---------------------

### Запрос

`GET /api/v1/purse/<код-валюты>`

### Ответ

Результат есть [`Cubux.Purse`][Cubux.Purse].

### Пример

Запрос:

    GET /api/v1/purse/RUB HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "amount":        37,
        "currency_code": "RUB"
    }


[Cubux.Purse]: ../type/purse.md
