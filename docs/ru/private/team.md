Команды
=======

Адрес: `/api/v1/team`


Список команд
-------------

### Запрос

`GET /api/v1/team`

### Ответ

Поле    | Тип
------- | -------------------
`items` | `Array<Cubux.Team>` (см. [`Cubux.Team`][Cubux.Team])

### Пример

Запрос:

    GET /api/v1/team HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 116
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id":                    37,
                "title":                 "Foo bar",
                "default_currency_code": "RUB",
                "time_zone":             "Europe/Moscow",
                "week_start":            null
            }
        ]
    }


Информация о команде
--------------------

### Запрос

`GET /api/v1/team/<ID-команды>`

### Ответ

Результат есть [`Cubux.Team`][Cubux.Team].

### Пример

Запрос:

    GET /api/v1/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "id":                    37,
        "title":                 "Foo bar",
        "default_currency_code": "RUB",
        "time_zone":             "Europe/Moscow",
        "week_start":            null
    }


Создание команды
----------------

### Запрос

`POST /api/v1/team`

### Ответ

Результат есть созданый объект [`Cubux.Team`][Cubux.Team].

### Пример

Запрос:

    POST /api/v1/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Type: application/json; charset=UTF-8
    Content-Length: 97

    {
        "title": "Foo bar"
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 104
    Content-Type: application/json; charset=UTF-8

    {
        "id":                    37,
        "title":                 "Foo bar",
        "default_currency_code": "RUB",
        "time_zone":             null,
        "week_start":            null
    }


Удаление команды
----------------

### Запрос

`DELETE /api/v1/team/<ID-команды>`

**Замечание:** Удалять команды могут только их Администраторы.

### Пример

Запрос:

    DELETE /api/v1/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0


[Cubux.Team]: ../type/team.md
