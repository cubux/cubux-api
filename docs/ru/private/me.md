Профайл
=======

Адрес: `/api/v1/me`


Данные профайла
---------------

### Запрос

`GET /api/v1/me`

### Ответ

Результат есть [`Cubux.Me`][Cubux.Me].

### Пример

Запрос:

    GET /api/v1/me HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 58
    Content-Type: application/json; charset=UTF-8

    {
        "id":           42,
        "mail":         "foobar@example.com",
        "name":         "Foo",
        "family":       "Bar",
        "country_code": "RUS",
        "username":     "Foo Bar"
    }


Изменение профайла
------------------

### Запрос

`PATCH /api/v1/me`

В теле запроса передаётся объект [`Cubux.Me`][Cubux.Me].

### Ответ

Результат есть обновлённый [`Cubux.Me`][Cubux.Me].

### Пример

Запрос:

    PATCH /api/v1/me HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd
    Content-Length: 63
    Content-Type: application/json; charset=UTF-8

    {
        "mail":         "foo@example.com",
        "name":         "Foo",
        "password":     "**********"
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 58
    Content-Type: application/json; charset=UTF-8

    {
        "id":           42,
        "mail":         "foo@example.com",
        "name":         "Foo",
        "family":       "Bar",
        "country_code": "RUS",
        "username":     "Foo Bar"
    }


Удаление профайла
-----------------

### Запрос

`DELETE /api/v1/me`

**Замечание:** Удаление невозможно, если существуют команды, где данный пользователь является
единственным администратором, но не единственным участником. Команды не могут оставаться без
администраторов.

### Пример

Запрос:

    DELETE /api/v1/me HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 204 No Content
    Content-Length: 0


[Cubux.Me]: ../type/me.md
