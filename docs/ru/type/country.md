Тип `Cubux.Country`
===================

Объект со следующими полями:

Поле                    | Тип          | Описание
----------------------- | ------------ | ----------------------------------
PK **`code`**           | строка       | Код из трех буков по [ISO 3166-1 alpha-3][iso-coutry-code]
`code2`                 | строка       | Код из двух буков по [ISO 3166-1 alpha-2][iso-coutry-code-2]
`title`                 | строка       | Название
`default_currency_code` | строка       | Код основной валюты (см. [валюты][])
`flag`                  | строка, NULL | Ссылка на флаг


[iso-coutry-code]: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3 "ISO 3166-1 alpha-3"
[iso-coutry-code-2]: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 "ISO 3166-1 alpha-2"
[валюты]: ../public/currency.md
