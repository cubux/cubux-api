Тип `Cubux.Currency`
====================

Объект со следующими полями:

Поле          | Тип          | Описание
------------- | ------------ | --------------------------------------------
PK **`code`** | строка       | Код из трех буков по [ISO][iso-currency-code]
`prefix`      | строка       | Префикс для форматирования суммы
`postfix`     | строка       | Суффикс для форматирования суммы
`symbol`      | строка       | Символ валюты
`title`       | строка       | Название валюты


[iso-currency-code]: https://en.wikipedia.org/wiki/ISO_4217 "ISO 4217 alpha-3"
