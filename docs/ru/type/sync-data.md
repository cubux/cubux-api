Тип `Cubux.Sync.Data`
=====================

Объект со следующими полями:

Поле                         | Тип                   | Описание
---------------------------- | --------------------- | ------------
`revision` _(только чтение)_ | целое                 | Номер версии
...                          | `Object`, NULL        | Поле соответствует всегда одной конкретной записи
...                          | `Array<Object>`, NULL | Поле соответствует множеству записей

Номер версии может быть и `0`, если никаких изменений ещё не было
зафиксировано вообще.

См. также:

*   [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable].
*   [`Cubux.Sync.GlobalData`][Cubux.Sync.GlobalData].


[Cubux.Sync.DataWriteable]: sync-data-writeable.md
[Cubux.Sync.GlobalData]: sync-global-data.md
