Тип `Cubux.Sync.GlobalData`
===========================

Объект, наследованый от [`Cubux.Sync.Data`][Cubux.Sync.Data], со следующими
_дополнительными_ полями:

Поле                   | Тип                                    | Описание
---------------------- | -------------------------------------- | ---------
`banks`                | `Array<Cubux.Bank>`, NULL              | Банки (см. [`Cubux.Bank`][Cubux.Bank])
`countries`            | `Array<Cubux.Country>`, NULL           | Страны (см. [`Cubux.Country`][Cubux.Country])
`currencies`           | `Array<Cubux.Currency>`, NULL          | Валюты (см. [`Cubux.Currency`][Cubux.Currency])
`currency_rates`       | `Array<Cubux.CurrencyRate>`, NULL      | Курсы валют (см. [`Cubux.CurrencyRate`][Cubux.CurrencyRate])
`providers`            | `Array<Cubux.Provider>`, NULL          | Провайдеры для связи с реальными банками (см. [`Cubux.Provider`][Cubux.Provider])
`target_pay_intervals` | `Array<Cubux.TargetPayInterval>`, NULL | Интервалы автосписания для целей (см. [`Cubux.TargetPayInterval`][Cubux.TargetPayInterval])

### Курсы валют `currency_rates`

Сервер отдаёт только непосредственные соотношения курсов, которыми
обладает. Для конвертации валюты _AAA_ в _BBB_ может потребоваться:

*   Прямой курс _AAA_ ⇒ _BBB_
*   Прямой курс _BBB_ ⇒ _AAA_
*   Курсы _AAA_ ⇔ _CCC_ и _CCC_ ⇔ _BBB_, где _CCC_ - произвольная третья
    валюта.


[Cubux.Bank]: bank.md
[Cubux.Country]: country.md
[Cubux.Currency]: currency.md
[Cubux.CurrencyRate]: currency-rate.md
[Cubux.Provider]: provider.md
[Cubux.Sync.Data]: sync-data.md
[Cubux.TargetPayInterval]: target-pay-interval.md
