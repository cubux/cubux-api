Тип `Cubux.Chart.Serie`
=======================

Объект со следующими полями:

Поле   | Тип                                                     | Описание
------ | ------------------------------------------------------- | --------
`data` | `Array<Cubux.Chart.Point>` (см [`Cubux.Chart.Point`][Cubux.Chart.Point]) | Последовательность значений

Пример:

```json
{
    "data": [
        {"y": 42},
        {"y": 37},
        {"y": 23.19}
    ]
}
```


[Cubux.Chart.Point]: chart-common-point.md
