Тип `Cubux.Sync.UserData`
=========================

Объект, наследованый от [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable], со следующими
_дополнительными_ полями:

Поле               | Тип                                  | Операции          | Описание
------------------ | ------------------------------------ | ----------------- | --------
`me`               | [`Cubux.Me`][Cubux.Me], NULL         | update, delete    | Данные профайла
`messages`         | `Array<Cubux.PostMessageFull>`, NULL | update, delete    | Уведомления (см. [`Cubux.PostMessageFull`][Cubux.PostMessageFull])
`teams` _(чтение)_ | `Array<Cubux.MyParticipation>`, NULL | _(только чтение)_ | Участие в командах (см. [`Cubux.MyParticipation`][Cubux.MyParticipation])
`teams` _(запись)_ | `Array<Cubux.MyTeam>`, NULL          | create            | Создание команд (см. [`Cubux.MyTeam`][Cubux.MyTeam])


[Cubux.Me]: ./me.md
[Cubux.MyParticipation]: ./my-participation.md
[Cubux.MyTeam]: ./my-team.md
[Cubux.PostMessageFull]: ./post-message-full.md
[Cubux.Sync.DataWriteable]: ./sync-data-writeable.md
