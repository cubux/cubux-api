Тип `Cubux.ProviderField`
=========================

Объект со следующими полями:

Поле             | Тип     | Описание
---------------- | ------- | --------
`name`           | строка  | Имя, используемое для отправки данных пользователя
`nature`         | enum `text`, `password`, `select`, `hidden` | Тип поля
`english_name`   | строка  | Человекопонятное название на английском языке
`localized_name` | строка  | Человекопонятное название на языке страны, которой принадлежит провайдер
`position`       | целое   | Порядковый номер поля в форме
`optional`       | boolean | Если `true`, то поле НЕобязательно для заполнения
`options`        | `Array<Cubux.ProviderFieldOption>`, NULL | Список вариантов для полей `select` (см. [`Cubux.ProviderFieldOption`][Cubux.ProviderFieldOption])


*Замечание:* Поля `hidden` указаны лишь у провайдеров с режимом `mode` равным
`oauth`.


[Cubux.ProviderFieldOption]: provider-field-option.md
