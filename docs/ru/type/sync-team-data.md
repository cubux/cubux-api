Тип `Cubux.Sync.TeamData`
=========================

Объект, наследованый от [`Cubux.Sync.DataWriteable`][Cubux.Sync.DataWriteable],
со следующими _дополнительными_ полями:

Поле              | Тип                                  | Операции               | Описание
----------------- | ------------------------------------ | ---------------------- | --------
`accounts`        | `Array<Cubux.Account>`, NULL         | create, update, delete | Счета (см. [`Cubux.Account`][Cubux.Account])
`accounts_access` | `Array<Cubux.AccountAccess>`, NULL   | create, update, delete | Привилегии участников по счетам (см. [`Cubux.AccountAccess`][Cubux.AccountAccess])
`budgets`         | `Array<Cubux.Budget>`, NULL          | create, update         | Бюджет (см. [`Cubux.Budget`][Cubux.Budget])
`categories`      | `Array<Cubux.Category>`, NULL        | create, update, delete | Категории (см. [`Cubux.Category`][Cubux.Category])
`drafts`          | `Array<Cubux.Draft>`, NULL           | create, update, delete | Чеки (см. [`Cubux.Draft`][Cubux.Draft])
`info`            | [`Cubux.Team`][Cubux.Team], NULL     | update, delete         | Информация о команде
`participants`    | `Array<Cubux.TeamParticipant>`, NULL | create, update, delete | Участники команды (см. [`Cubux.TeamParticipant`][Cubux.TeamParticipant])
`plans`           | `Array<Cubux.Plan>`, NULL            | create, update, delete | Запланированные транзакции (см. [`Cubux.Plan`][Cubux.Plan])
`targets`         | `Array<Cubux.Target>`, NULL          | create, update, delete | Цели (см. [`Cubux.Target`][Cubux.Target])
`transactions`    | `Array<Cubux.TransactionInfo>`, NULL | create, update, delete | Транзакции (см. [`Cubux.TransactionInfo`][Cubux.TransactionInfo])

**Замечание:** Изменение своих привилегий по счетам (`accounts_access`) и
своего участия в команде (`participants`) невозможно.

**Замечание:** Служебные категории (`categories`) не могут быть удалены.


[Cubux.Account]: account.md
[Cubux.AccountAccess]: account-access.md
[Cubux.Budget]: budget.md
[Cubux.Category]: category.md
[Cubux.Draft]: draft.md
[Cubux.Plan]: plan.md
[Cubux.Sync.DataWriteable]: sync-data-writeable.md
[Cubux.Target]: target.md
[Cubux.Team]: team.md
[Cubux.TeamParticipant]: team-participant.md
[Cubux.TransactionInfo]: transaction-info.md
