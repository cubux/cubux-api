Тип `Cubux.PostMessageFull`
===========================

Объект со следующими полями:

Поле                                  | Тип               | Описание
------------------------------------- | ----------------- | --------
`message_id` **PK** _(только чтение)_ | целое             | ID
`created_at`        _(только чтение)_ | дата и время ISO  | Дата создания
`is_new`                              | boolean           | `true` - новое, непрочитаное
`translations`      _(только чтение)_ | `Array<Cubux.PostTranslation>` | Контент сообщения на нескольких языках (см [`Cubux.PostTranslation`][Cubux.PostTranslation])

**Замечание**: Обновление доступно только для поля `is_new`.


[Cubux.PostTranslation]: ./post-translation.md
