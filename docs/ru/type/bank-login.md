Тип `Cubux.BankLogin`
=====================

Объект со следующими полями:

Поле                       | Тип                       | Описание
-------------------------- | ------------------------- | --------
PK **`id`**                | строка                    | ID
`provider_id`              | целое                     | ID провайдера
`status`                   | enum `inactive`, `active` | `inactive` в процессе подключения; `active` после завершения подключения
`stage`                    | enum, NULL                | Этап подключения (см. ниже)
`interactive`              | `Cubux.BankLoginInteractive` (см. ниже), NULL | Текущий интерактивный шаг
`daily_refresh`            | boolean                   | Разрешено ли производить автоматическое обновление данных
`last_attempt_fail`        | строка, NULL              | Сообщение о последней ошибке
`last_success_at`          | дата-время, NULL          | Дата и время последнего успешного сеанса
`next_refresh_possible_at` | дата-время, NULL          | Дата и время, когда будет разрешено запросить обновление данных. Поле может быть NULL, если провайдер не поддерживает автоматическое обновление, либо логин обрабатывается в настоящее время.


### Этапы подключения

*   `interactive` - Интерактивное подтверждение аутентификации.
*   `fetch_accounts` - Получение счетов.
*   `fetch_recent` - Получение транзакций.


### Тип `Cubux.BankLoginInteractive`

Объект со следующими полями:

Поле         | Тип          | Описание
------------ | ------------ | --------
`expires_at` | дата-время   | Срок жизни
`fields`     | массив строк | Имена интерактивных полей, которые нужно заполнить и отправить
`html`       | строка       | HTML код, например, с капчей
