Тип `Cubux.ProviderFieldOption`
===============================

Объект со следующими полями:

Поле             | Тип     | Описание
---------------- | ------- | --------
`name`           | строка  | Имя, уникальное среди соседних объектов
`english_name`   | строка  | Человекопонятный текст на английском языке
`localized_name` | строка  | Человекопонятный текст на языке страны, которой принадлежит провайдер
`option_value`   | строка  | Значение, которое необходимо использовать при отправке на данных пользователя в случае выбора этого варианта
`selected`       | boolean | Выбран ли данный вариант по умолчанию
