Тип `Cubux.Chart.Column`
========================

Объект со следующими полями:

Поле         | Тип                                                     | Описание
------------ | ------------------------------------------------------- | --------
`categories` | `Array<string>`                                         | Названия столбцов
`series`     | `Array<Cubux.Chart.Serie>` (см [`Cubux.Chart.Serie`][Cubux.Chart.Serie]) | Список последовательностей данных

Поле `series` предназначено для нескольких наборов данных.

Пример:

```json
{
    "categories": [
        "2015",
        "2016",
        "2017"
    ],
    "series": [
        {
            "data": [127, 311, 223]
        },
        {
            "data": [599, 409, 719]
        }
    ]
}
```


[Cubux.Chart.Serie]: chart-common-serie.md
