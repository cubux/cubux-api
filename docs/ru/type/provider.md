Тип `Cubux.Provider`
====================

Объект со следующими полями:

Поле              | Тип     | Описание
----------------- | ------- | --------
PK **`id`**       | целое   | ID
`name`            | строка  | Название на языке страны
`code`            | строка  | Уникальное имя для технических нужд
`mode`            | enum `api`, `web`, `oauth` | Способ аутентификации (см. [варианты авторизации][provider-modes])
`fields`          | `Cubux.ProviderFieldset` (см. ниже) | Описание полей
`automatic_fetch` | boolean | Возможно ли автоматическое обновление данных для Логинов
`home_url`        | строка  | URL домашней страницы банка
`instruction`     | строка  | Текст, поясняющий пользователю, что нужно сделать, чтобы авторизовать приложение
`interactive`     | boolean | Требует ли провайдер интерактивного подтверждения для аутентификации


### Тип `Cubux.ProviderFieldset`

Объект со следующими полями:

Поле           | Тип                          | Описание
-------------- | ---------------------------- | --------
`required`     | `Array<Cubux.ProviderField>` | Поля для первого этапа создания логина (см. [`Cubux.ProviderField`][Cubux.ProviderField])
`interactive`  | `Array<Cubux.ProviderField>` | Поля, участвующие в интерактивных этапах (см. [`Cubux.ProviderField`][Cubux.ProviderField])


[Cubux.ProviderField]: provider-field.md
[provider-modes]: ../bank-integration/provider-modes.md
