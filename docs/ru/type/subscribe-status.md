Тип `Cubux.SubscribeStatus`
===========================

Объект со следующими полями:

Поле       | Тип              | Описание
---------- | ---------------- | --------
`type`     | строка, NULL     | Тип подписки (см. [`Cubux.SubscribePlan`][Cubux.SubscribePlan]). NULL означает бесплатную подписку по умолчанию.
`deadline` | дата-время, NULL | Дата и время отключения связей с банками, если не подлить подписку
`features` | `Array<Cubux.SubscribeFeature>` (см. [`Cubux.SubscribeFeature`][Cubux.SubscribeFeature]) | Состояния особенностей подписки
`until`    | дата-время, NULL | Срок окончания пописки

[Cubux.SubscribeFeature]: ./subscribe-feature.md
[Cubux.SubscribePlan]: ./subscribe-plan.md
