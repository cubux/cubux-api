Тип `Cubux.Bank`
================

Объект со следующими полями:

Поле           | Тип          | Описание
-------------- | ------------ | -----------------
PK AI **`id`** | целое        | ID
`country_code` | строка       | Трехбуквеный код страны [`Cubux.Country`][Cubux.Country] из поля `code`
`name`         | строка       | Название
`logo`         | строка, NULL | Ссылка на логотип
`provider_id`  | целое, NULL  | ID провайдера [`Cubux.Provider`][Cubux.Provider] для связи с реальным банком


Если поле `provider_id` заполнено, то для счетов этого банка возможно настроить
связь с соответствующим реальным банком, информация о котором хранится в данных
о провайдерах [`Cubux.Provider`][Cubux.Provider].
См. [Связь с реальными банками][bank-integration].


[Cubux.Country]: country.md
[Cubux.Provider]: provider.md
[bank-integration]: ../bank-integration/README.md
