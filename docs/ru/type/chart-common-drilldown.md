Тип `Cubux.Chart.Drilldown`
===========================

Объект со следующими полями:

Поле   | Тип                                                     | Описание
------ | ------------------------------------------------------- | --------
`id`   | строка                                                  | ID (используется в родительской [`Cubux.Chart.Point`][Cubux.Chart.Point]
`data` | `Array<Cubux.Chart.Point>` (см [`Cubux.Chart.Point`][Cubux.Chart.Point]) | Последовательность значений
`name` | строка                                                  | Название последовательности

Пример:

```json
{
    "id": "42",
    "name": "Foo bar",
    "data": [127, 311, 223]
}
```


[Cubux.Chart.Point]: chart-common-point.md
