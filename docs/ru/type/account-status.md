Тип `Cubux.AccountStatus`
=========================

Объект со следующими полями:

Поле                           | Тип     | Описание
------------------------------ | ------- | ------------------------
PK **`account_id`**            | целое   | ID счёта (см. [счета][])
`amount`                       | дробное | Средства на счёте


[счета]: ../team/account.md
