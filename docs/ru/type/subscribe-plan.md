Тип `Cubux.SubscribePlan`
=========================

Объект со следующими полями:

Поле       | Тип    | Описание
---------- | ------ | --------
`type`     | строка | Тип подписки
`features` | `Array<Cubux.SubscribeFeature>` | Состояния особенностей подписки (см. [`Cubux.SubscribeFeature`][Cubux.SubscribeFeature])
`prices`   | `Array<Cubux.SubscribePrice>`   | Цены подписки (см. [`Cubux.SubscribePrice`][Cubux.SubscribePrice])

### Типы подписок

*   `premuim` - Премиум подписка


[Cubux.SubscribeFeature]: ./subscribe-feature.md
[Cubux.SubscribePrice]: ./subscribe-price.md
