Тип `Cubux.Sync.DataWriteable`
==============================

Объект, наследованый от [`Cubux.Sync.Data`][Cubux.Sync.Data], со следующими
_дополнительными_ полями:

Поле     | Тип                             | Описание
-------- | ------------------------------- | --------
`errors` | `Array<Cubux.Sync.Error>`, NULL | Ошибки, возникшие при принятии изменений (см. [`Cubux.Sync.Error`][Cubux.Sync.Error])

Поля данных могут быть NULL в ответах, несущих только изменения.

См. также:

*   [`Cubux.Sync.TeamData`][Cubux.Sync.TeamData].
*   [`Cubux.Sync.UserData`][Cubux.Sync.UserData].


[Cubux.Sync.Data]: sync-data.md
[Cubux.Sync.Error]: sync-error.md
[Cubux.Sync.TeamData]: sync-team-data.md
[Cubux.Sync.UserData]: sync-user-data.md
