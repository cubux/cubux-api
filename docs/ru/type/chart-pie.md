Тип `Cubux.Chart.Pie`
=====================

Объект со следующими полями:

Поле        | Тип                                                             | Описание
----------- | --------------------------------------------------------------- | -------------------------------
`drilldown` | `Array<Cubux.Chart.Drilldown>` (см [`Cubux.Chart.Drilldown`][Cubux.Chart.Drilldown]) | Наборы детализации
`series`    | `Array<Cubux.Chart.Serie>` (см [`Cubux.Chart.Serie`][Cubux.Chart.Serie])         | Список последовательностей данных

Поле `series` предназначено для нескольких наборов данных.

Пример:

```json
{
    "series": [
        {
            "data": [
                {
                    "y": 127,
                    "name": "Авто"
                },
                {
                    "y": 311,
                    "name": "Еда",
                    "drilldown": "42"
                },
                {
                    "y": 223,
                    "name": "Здоровье"
                }
            ]
        }
    ],
    "drilldown": [
        {
            "id": "42",
            "name": "Еда",
            "data": [
                ["Хлеб", 8],
                ["Молоко", 11],
                ["Колбаса", 23]
            ]
        }
    ]
}
```


[Cubux.Chart.Drilldown]: chart-common-drilldown.md
[Cubux.Chart.Serie]: chart-common-serie.md
