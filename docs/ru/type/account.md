Тип `Cubux.Account`
===================

Объект со следующими полями:

Поле             | Тип                     | Описание
---------------- | ----------------------- | ------------------------------------------
PK AI **`id`**   | целое                   | ID
`name`           | строка                  | Название
`country_code`   | строка                  | Код страны (см [страны][])
`currency_code`  | строка                  | Код валюты (см [валюты][])
`bank_id`        | целое, NULL             | ID банка (см [банки][]). Если не указано, то "наличные".
`initial_amount` | дробное, NULL           | Сумма начального остатка
`initial_date`   | дата `yyyy-mm-dd`, NULL | Дата транзакции для первоначального остатка
`sort`           | целое                   | Порядковый номер для сортировки. Произвольные числа. Рекомендуется использовать шаг больше 1 для более удобной вставки между соседями. Например: 10, 20, 30, ... или 100, 200, 300, ... Перенумерация соседей при изменении не обязательна, но уместна.
`is_locked` _(только чтение)_        | boolean         | Счет блокирован (см. ниже)
`auth_id`                            | целое, NULL     | ID [аутентификации][Cubux.BankAuth] для связи с реальным банком
`login_id` _(только чтение)_         | целое, NULL     | ID Логина [`Cubux.BankLogin`][Cubux.BankLogin] для синхронизации с реальным банком
`daily_refresh` _(только чтение)_    | boolean, NULL   | Разрешено ли автоматическое обновление данных для синхронизации с реальным банком
`next_refresh_possible_at` _(только чтение)_ | дата-время `yyyy-mm-ddThh:ii:ssZ`, NULL | Когда будет разрешено запросить обновление данных при синхронизации с реальным банком.
`last_sync_date` _(только чтение)_   | дата-время `yyyy-mm-ddThh:ii:ssZ`, NULL | Время последнего сеанса синхронизации с реальным банком
`is_sync_lost` _(только чтение)_     | boolean         | для счетов, связнными с реальными банками, значение `true` говорит о том, что связь утрачена (см. поле `sync_lost_reason`)
`sync_lost_reason` _(только чтение)_ | enum \*1), NULL | для счетов, связнными с реальными банками, код причины утраты связи

**1)** Код причины утраты связи может иметь следующие значения:

Код                               | Описание
--------------------------------- | --------
`interactive_timeout`             | Срок жизни интерактивного подтверждения истёк
`invalid_credentials`             | Учетный данные (обычно логин и пароль) неверны
`invalid_interactive_credentials` | Данные интерактивного подтверждения (например, код из СМС) неверны
`oauth_token`                     | Токен доступа устарел или отозван
`remote_account_deleted`          | Целевой счёт не существует
`remote_auth_outdated`            | Выданные разрешения устарели или отозваны, либо идентификационные данные пользователя устарели


### Блокированый счёт

Блокированый счёт не может быть использован для проведения операций. Например,
для счёта настроена интеграция с банком, откуда импортируются реальные операции.


[Cubux.BankAuth]: bank-auth.md
[Cubux.BankLogin]: bank-login.md
[банки]:  ../public/bank.md
[валюты]: ../public/currency.md
[страны]: ../public/country.md
