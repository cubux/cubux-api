Процедура установления связи
============================

Если при создании счёта пользователь выбрал банк [`Cubux.Bank`][Cubux.Bank],
для которого установлена связь с провайдером [`Cubux.Provider`][Cubux.Provider],
то Приложение предоставляет пользователю выбор: нужна ли связь с реальным банком
или нет.

Если Пользователь отказался, выбрав "нет", то Приложение предлагает создать один
обычный счёт, заполнив остальные необходимые поля.

Если же Пользователь выбрал вариант "да", то для установления связи с банком
Пользователь с помощью приложения должен пройти следующие шаги:


### 1. Аутентификация

Аутентификация согласно [способу][provider-modes], по котором работает выбраный
Провайдер [`Cubux.Provider`][Cubux.Provider]. Итогом этого шага является Логин
[`Cubux.BankLogin`][Cubux.BankLogin].

Например, был выбран Банк:

    {
        "id":           14,
        "country_code": "RUS",
        "provider_id":  67,
        "name":         "...",
        "logo":         null
    }

Он связан со следующим Провайдером (Провайдеры описаны в глобальных данных в АПИ
синхронизации):

    {
        "id":              67,
        "mode":            "web",
        "automatic_fetch": false,
        "interactive":     true,
        ...
        "fields": {
            "required": [
                {
                    "name":           "login",
                    "nature":         "text",
                    "english_name":   "Login",
                    "localized_name": "Login",
                    "position":       1,
                    "optional":       false,
                    "options":        null
                },
                {
                    "name":           "password",
                    "nature":         "password",
                    "english_name":   "Password",
                    "localized_name": "Password",
                    "position":       2,
                    "optional":       false,
                    "options":        null
                }
            ],
            "interactive": [
                {
                    "name":           "sms",
                    "nature":         "text",
                    "english_name":   "SMS code",
                    "localized_name": "SMS code",
                    "position":       1,
                    "optional":       false,
                    "options":
                    null
                }
            ]
        }
    }

В форме аутентификации необходимо показать поля, описаные в массиве
`fields.required` данного провайдера: текстовое поле c названием `Login` и поле
для пароля с названием `Password`. Оба поля обязательны для заполнения
(`optional` = `false`).

Для отправки данных необходимо использовать запрос создания Логина, используя
имена Полей (значение атрибута `name` в описании Полей): `name` и `password`
соответственно:

    POST /api/v1/se/login/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
        "country_code":  "RUS",
        "provider_id":   67,
        "credentials": {
            "login": "demo",
            "pass":  "********"
        }
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id":                "172380",
        "provider_id":       67,
        "status":            "inactive",
        "daily_refresh":     false,
        "last_attempt_fail": null,
        "last_success_at":   null,
        "next_refresh_possible_at": null,
        "stage":             null,
        "interactive":       null
    }

Логин с `id` = `172380` будем использовать далее в примерах.


### 2. Прогресс подключения

Приложение показывает Пользователю информацию о том, что происходит установление
связи с банком. В это время Приложение периодически (например, каждые 2—5
секунд) проверяет состояние Логина, используя запрос информации о Логине из
[API Логинов][bank-login-api].

Продолжая серию примеров для Логина `172380`, запрос информации:

    GET /api/v1/se/login/team/37/172380 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id":                "172380",
        "provider_id":       67,
        "status":            "inactive",
        "daily_refresh":     false,
        "last_attempt_fail": null,
        "last_success_at":   null,
        "next_refresh_possible_at": null,
        "stage":             null,
        "interactive":       null
    }

Пока код ответа сигнализирует об успехе (200) и этап `stage` не достиг
`interactive` или `fetch_accounts` (или далее), выжидаем небольшой тайамут,
например, в 2—5 секунд и повторяем запрос.

Код ответа может стать 410. Это делается в случае некорректных данных
аутентификации. В этом случае в теле ответа также содержится описание Логина, но
на этот раз поле `last_attempt_fail` может быть заполнено сообщением об ошибке.


### 3. Интерактивное подтверждение

Если логин переходит в интерактивную фазу (например, подтверждение по СМС или
капча), Приложение предлагает Пользователю заполнить необходимые поля и
отправляет введённые данные в запросе интерактивного этапа из
[API Логинов][bank-login-api]. После отправки этих данных Приложение вновь
показывает прогресс подключение.

В нашем примере Провайдер подразумевает подтверждение аутентификации с помощью
кода, отправленного в СМС. В Провайдере описано интерактивное поле `sms`:

    {
        "id":          67,
        "interactive": true,
        ...
        "fields": {
            ...
            "interactive": [
                {
                    "name":           "sms",
                    "nature":         "text",
                    "english_name":   "SMS code",
                    "localized_name": "SMS code",
                    "position":       1,
                    "optional":       false,
                    "options":
                    null
                }
            ]
        }
    }

Когда потребуется ввод и отправка кода, информация о логине (которая до этого
момента периодически проверяется, как описано в предыдущем пункте) примет таков
вид:

    {
        "id":                "172380",
        "provider_id":       67,
        "status":            "inactive",
        "daily_refresh":     false,
        "last_attempt_fail": null,
        "last_success_at":   "2017-01-01T01:23:45Z",
        "next_refresh_possible_at": "2017-02-01T12:34:56Z",
        "stage":             "interactive",
        "interactive": {
            "html":       "",
            "expires_at": "2017-01-20T03:15:07Z",
            "fields":     ["sms"]
        }
    }

Здесь сказано, что ожидаются интерактивные данные для поля с именем `sms`.

Приложение выводит форму с текстовым полем, согласно описанию Поля `sms` выше в
данных Привайдера.

После ввода и отправки кода пользователем, Приложение отправляет интерактивные
данные:

    PATCH /api/v1/se/login/interactive/team/37/172380 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
        "credentials": {
            "sms": "123456"
        }
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id":                "172380",
        "provider_id":       67,
        "status":            "inactive",
        "daily_refresh":     false,
        "last_attempt_fail": null,
        "last_success_at":   null,
        "next_refresh_possible_at": null,
        "stage":             "interactive",
        "interactive":       null
    }

Далее Приложение снова возвращается к предыдущему пункту и показывает прогресс
подключения.

В случае некорректных данных результат зависит от Провайдера. Либо код ответа
при отправке интерактивных данных будет 409, либо позже выяснится, что Логин
остался на этапе `stage` = `interactive`, но поле `last_attempt_fail`
заполнилось сообщением об ошибке.


### 4. Начало получения данных

Когда логин переходит в фазу получения данных, Приложение может показать в
своём интерфейсе информацию об этом. Например, закрыть диалог создания счёта
и вывести рядом со списком счетов информацию о том, что выполняется
получение данных от такого-то банка.

Итак, в нашем примере в пункте 2 выше из информации о логине мы вдруг узнаём,
что этап `stage` стал `fetch_accounts` или один из последующих (см. этапы в
[`Cubux.BankLogin`][Cubux.BankLogin]). Это значит, про процесс аутентификации
прошел успешно и началось получение необходимых данных. Диалоги создания счёта
и прогресса подключения можно закрыть.

Однако, пока получение данных не завершено, конечные счета не будут созданы
сервером. Чтобы показать Пользователю факт о том, что по таким-то Банкам ещё не
завершено получение данных, можно использовать запрос обрабатываемых логинов:

    GET /api/v1/se/login/schedule/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id": "172380",
                "provider_id": 68,
                "status": "inactive",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": null,
                "next_refresh_possible_at": null,
                "stage": "fetch_accounts",
                "interactive": null
            },
            {
                "id": "174168",
                "provider_id": 2315,
                "status": "inactive",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": null,
                "next_refresh_possible_at": null,
                "stage": "fetch_recent",
                "interactive": null
            }
        ]
    }



### 5. Конец подключения и создание счетов

Когда первоначальное получение данных завершится, будут созданы новые счета,
которые на стороне сервера будут связаны с соответствующими реальными
счетами логина. Приложению следует обновить данные о счетах, чтобы показать
новые счета Пользователю в своём интерфейсе.


### Получение транзакций

Получение транзакций по новым счетам будет завершено позже в рамках
периодической синхронизации. Хотя бы часть транзакций будет доступна в течение
5 минут. Получиться данные можно путём обновления истории транзакций.


[Cubux.Bank]: ../type/bank.md
[Cubux.BankLogin]: ../type/bank-login.md
[Cubux.Provider]: ../type/provider.md
[bank-login-api]: api-login.md
[provider-modes]: provider-modes.md
