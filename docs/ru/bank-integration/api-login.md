Логины
======


Список Логинов Команды
----------------------

### Запрос

`GET /api/v1/se/login/team/<ID-команды>`

Параметры:

Параметр      | Тип                       | По умолчанию | Описание
------------- | ------------------------- | ------------ | --------
`provider_id` | целое                     | _(нет)_      | Фильтрация про ID провайдера
`status`      | enum `inactive`, `active` | _(нет)_      | Фильтрация по статусу

### Ответ

Поле    | Тип
------- | ---
`items` | `Array<Cubux.BankLogin>` (см. [`Cubux.BankLogin`][Cubux.BankLogin])

### Пример

Запрос:

    GET /api/v1/se/login/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id": "172380",
                "provider_id": 68,
                "status": "active",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": "2017-01-01T01:23:45Z",
                "next_refresh_possible_at": "2017-02-01T12:34:56Z",
                "stage": null,
                "interactive": null
            },
            {
                "id": "174168",
                "provider_id": 2315,
                "status": "active",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": "2017-01-01T01:23:45Z",
                "next_refresh_possible_at": "2017-02-01T12:34:56Z",
                "stage": null,
                "interactive": null
            }
        ]
    }


Список новых обаратываемых Логинов Команды
------------------------------------------

Новые логины, по которым началось, но ещё не завершилось получение данных. Новые
счета будут созданы только после завершения получения данных.

### Запрос

`GET /api/v1/se/login/schedule/team/<ID-команды>`

### Ответ

Поле    | Тип
------- | ---
`items` | `Array<Cubux.BankLogin>` (см. [`Cubux.BankLogin`][Cubux.BankLogin])

### Пример

Запрос:

    GET /api/v1/se/login/schedule/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "items": [
            {
                "id": "172380",
                "provider_id": 68,
                "status": "inactive",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": null,
                "next_refresh_possible_at": null,
                "stage": "fetch_accounts",
                "interactive": null
            },
            {
                "id": "174168",
                "provider_id": 2315,
                "status": "inactive",
                "daily_refresh": false,
                "last_attempt_fail": null,
                "last_success_at": null,
                "next_refresh_possible_at": "2017-02-01T12:34:56Z",
                "stage": "fetch_recent",
                "interactive": null
            }
        ]
    }


Информация о Логине
-------------------

### Запрос

`GET /api/v1/se/login/team/<ID-команды>/<ID-логина>`

### Ответ

В случае успеха результат есть [`Cubux.BankLogin`][Cubux.BankLogin].

Ошибки:

Код | Описание
--- | --------
410 | Логин удалён. Тело ответа есть [`Cubux.BankLogin`][Cubux.BankLogin], где может быть заполнено поле `last_attempt_fail`, откуда можно узнать причину удаления.

### Пример

Запрос:

    GET /api/v1/se/login/team/37/172380 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id": "172380",
        "provider_id": 68,
        "status": "active",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": "2017-01-01T01:23:45Z",
        "next_refresh_possible_at": "2017-02-01T12:34:56Z",
        "stage": null,
        "interactive": null
    }


Создание Логина
---------------

Данный запрос используется для провайдеров, работающих в режимая `api` и `web`.

### Запрос

`POST /api/v1/se/login/team/<ID-команды>`

В теле запроса передается объект со следующими полями:

Поле                 | Тип     | По умолчанию         | Описание
-------------------- | ------- | -------------------- | --------
`country_code`       | строка  | (_обязательно_)      | Трехбуквеный код страны, которой принадлежит провайдер
`provider_id` \*1)   | целое   | (_обязательно_) \*1) | ID провайдера
`provider_code` \*1) | строка  | (_обязательно_) \*1) | Символьный код (поле `code`) провайдера
`credentials`        | объект  | (_обязательно_)      | Учётные данные Пользователя. Имена свойств объекта соответствуют именам полей первого этапа, описаных в данных провайдера в массиве `fields.required`. Значения свойств - соответствующие значения, введённые Пользователем. 
`daily_refresh`      | boolean | `false`              | Запрашивать ли обновление данных автоматически. Используется только для Провайдеров, у которых поле `automatic_fetch` есть `true`. Для остальных провайдеров роли не играет. По умолчанию `false`.

**1)** Поля `provider_id` и `provider_code` могут быть указаны как оба сразу,
так и только одно из них.

### Ответ

В случае успеха результат есть [`Cubux.BankLogin`][Cubux.BankLogin].

Ошибки:

Код | Описание
--- | --------
409 | Логин с такими учётными данными уже существует в данной команде

### Пример

Предположим, что есть такой провайдер:

    {
        "id":              12345,
        "automatic_fetch": true,
        "interactive":     false,
        "fields": {
            "required": [
                {
                    "name": "login",
                    "nature": "text",
                    "english_name": "Login",
                    "localized_name": "Логин",
                    "position": 0,
                    "optional": false,
                    "options": null
                },
                {
                    "name": "pass",
                    "nature": "password",
                    "english_name": "Password",
                    "localized_name": "Пароль",
                    "position": 1,
                    "optional": false,
                    "options": null
                },
            ]
        },
        ...
    }

Создание логина для этого провайдера будет выглядеть так:

Запрос:

    POST /api/v1/se/login/team/37 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
        "country_code": "RUS",
        "provider_id": 12345,
        "credentials": {
            "login": "demo",
            "pass": "********"
        }
    }

Ответ:

    HTTP/1.1 201 Created
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id": "172380",
        "provider_id": 12345,
        "status": "inactive",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": null,
        "next_refresh_possible_at": null,
        "stage": null,
        "interactive": null
    }


Интерактивное подтверждение
---------------------------

Если банк для аутентификации требует интерактивного подтверждения (например, СМС
или капча), то логин в процессе установки связи перейдёт в интерактивный этап.
В этом случае поле `status` логина будет равно `inactive`, поле `stage` равно
`interactive` и поле `interactive` будет содержать данные о текущем
интерактивном шаге (см. `Cubux.BankLoginInteractive` в описании
[`Cubux.BankLogin`][Cubux.BankLogin]). В частности, там будут указаны имена
инерактивных полей, которые необходимо отправить в запросе. Интерактивные поля
провайдера описаны в его массиве `fields.interactive`.

### Запрос

`PATCH /api/v1/se/login/interactive/team/<ID-команды>/<ID-логина>`

В теле запроса передается объект со следующими полями:

Поле            | Тип    | Описание
--------------- | ------ | --------
`credentials`   | объект | Данные, необходимые для прохождения данного интерактивного шага. Имена свойств объекта соответствуют именам, указаным в информации о текущем интерактивном шаге в Логине, и описаным в провайдере в массиве `fields.interactive`. Значения свойств - соответствующие значения, введённые Пользователем.

### Ответ

Результат есть [`Cubux.BankLogin`][Cubux.BankLogin].

### Пример

Предположим, что есть такой провайдер:

    {
        "id":              12345,
        "automatic_fetch": false,
        "interactive":     true,
        "fields": {
            ...,
            "interactive": [
                {
                    "name": "sms",
                    "nature": "text",
                    "english_name": "Code from SMS",
                    "localized_name": "Код из СМС",
                    "position": 0,
                    "optional": false,
                    "options": null
                },
                {
                    "name": "captcha",
                    "nature": "text",
                    "english_name": "Code on picture",
                    "localized_name": "Код на картинке",
                    "position": 0,
                    "optional": false,
                    "options": null
                },
            ]
        },
        ...
    }

Предположим, что созданые логин перешел в интерактивную фазу и имеет следующий
вид:

    {
        "id": "172380",
        "provider_id": 12345,
        "status": "inactive",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": null,
        "next_refresh_possible_at": null,
        "stage": null,
        "interactive": {
            "expires_at": "2017-01-09T12:34:56Z",
            "fields": ["sms"],
            "html": ""
        }
    }

Это значит, что для дальнейшего продвижения неодходимо отправить интерактивные
данные: поле `sms`.

Запрос:

    PATCH /api/v1/se/login/interactive/team/37/172380 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
        "credentials": {
            "sms": "123456"
        }
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id": "172380",
        "provider_id": 12345,
        "status": "inactive",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": null,
        "next_refresh_possible_at": null,
        "stage": null,
        "interactive": null
    }


Запрос обновления данных
------------------------

Если автоматическое обновление данных не поддерживается провайдером, либо было
отключено для данного Логина, то получение новых данных возможно только после
запроса обновлений.

### Запрос

`PATCH /api/v1/se/login/refresh/team/<ID-команды>/<ID-логина>`

В теле запроса передается объект со следующими полями:

Поле            | Тип     | Описание
--------------- | ------- | --------
`daily_refresh` | boolean | Запрашивать ли обновление данных автоматически. Используется только для Провайдеров, у которых поле `automatic_fetch` есть `true`. Для остальных провайдеров роли не играет. По умолчанию `false`.

### Ответ

Результат есть [`Cubux.BankLogin`][Cubux.BankLogin].

Ошибки:

Код | Описание
--- | --------
409 | Обновление данных не может быть произведено. Возможно, после последнее получения данных ещё не прошло достаточно времени, либо получение уже выполняется сейчас.

### Пример

Запрос:

    PATCH /api/v1/se/login/refresh/team/37/39478 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id": "39478",
        "provider_id": 12345,
        "status": "inactive",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": "2017-01-01T01:23:45Z",
        "next_refresh_possible_at": "2017-02-01T12:34:56Z",
        "stage": null,
        "interactive": null
    }


Повторное подключение
---------------------

Повторное подключение предназначено для обновления учётных данных, например,
в случае изменения пароля (для провайдеров с режимами `api` и `web`) или в
случае отзыва токена доступа (для провайдеров с режимом `oauth`) владельцем
реального логина.

В этом случае просто создать новый логин с тем же идентификатором пользователя
(имя пользователя / логин на стороне провайдера) не выйдет, т.к. такое
дублирование логинов не разрешено.

### Запрос

`PATCH /api/v1/se/login/reconnect/team/<ID-команды>/<ID-логина>`

В теле запроса передается объект со следующими полями:

Поле            | Тип     | Описание
--------------- | ------- | --------
`credentials`   | объект  | Учётные данные Пользователя. Имена свойств объекта соответствуют именам полей первого этапа, описаных в данных провайдера в массиве `fields.required`. Значения свойств - соответствующие значения, введённые Пользователем. 
`daily_refresh` | boolean | Запрашивать ли обновление данных автоматически. Используется только для Провайдеров, у которых поле `automatic_fetch` есть `true`. Для остальных провайдеров роли не играет. По умолчанию `false`.

### Ответ

Результат есть [`Cubux.BankLogin`][Cubux.BankLogin].

Ошибки:

Код | Описание
--- | --------
409 | Идентификатор пользователя (имя пользователя / логин) на стороне провайдера отличается от тех, что были указаны при создании данного Логина. Необходимо либо использовать прежний идентификатор пользователя, указав новый пароль, либо создать новый логин с новым идентификатором пользователя.

### Пример

Предположим, что есть такой провайдер:

    {
        "id":              12345,
        "automatic_fetch": true,
        "interactive":     false,
        "fields": {
            "required": [
                {
                    "name": "login",
                    "nature": "text",
                    "english_name": "Login",
                    "localized_name": "Логин",
                    "position": 0,
                    "optional": false,
                    "options": null
                },
                {
                    "name": "pass",
                    "nature": "password",
                    "english_name": "Password",
                    "localized_name": "Пароль",
                    "position": 1,
                    "optional": false,
                    "options": null
                },
            ]
        },
        ...
    }

Предположим, что при создании было использвано имя пользователя `demo`, и
Логин создался с id `39478`.

В этом случае запрос повторного подключения будет выглядеть так:

Запрос:

    PATCH /api/v1/se/login/reconnect/team/37/39478 HTTP/1.1
    Host: www.cubux.net
    Authorization: Bearer 617babbbee3cbe4ee28e8c440d405be3c39ea2bd

    {
        "credentials": {
            "login": "demo",
            "pass": "********"
        }
    }

Ответ:

    HTTP/1.1 200 OK
    Content-Length: 225
    Content-Type: application/json; charset=UTF-8

    {
        "id": "39478",
        "provider_id": 12345,
        "status": "inactive",
        "daily_refresh": false,
        "last_attempt_fail": null,
        "last_success_at": "2017-01-01T01:23:45Z",
        "next_refresh_possible_at": "2017-02-01T12:34:56Z",
        "stage": null,
        "interactive": null
    }


[Cubux.BankLogin]: ../type/bank-login.md
