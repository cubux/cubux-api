CUBUX API
=========

> API синхронизации v1 более не поддерживается с 01.07.2021.
> См. [API синхронизации v2](https://github.com/cubux-net/cubux-net-api-v2).

Обзор
-----

API [cubux.net][] устроено по принципу REST.

Базовый адрес для API запросов: `https://www.cubux.net/`. В документации
даны относительные адреса от базового.

Все необязательные параметры запросов указываются в строке параметров
запроса (query string).

Данные в теле запроса могут быть закодированы в `application/json`,
`application/x-www-form-urlencoded` и `multipart/form-data`.

Ответ отдается в формате JSON или XML согласно заголовку `Accept` (по
умолчанию — JSON). Формат JSON предпочтительнее.

Формат XML _не рекомендован_ к использованию, поскольку не был подробно
документирован. Формат XML будет упразднён в будущем.


Содержание
----------

*   [Аутентификация](auth/README.md)
    *   [Запрос токена доступа](auth/request.md)
    *   [Разрешения (scope)](auth/scopes.md)
    *   [Способы предоставления токена доступа](auth/grant_types.md)
*   [Пользователи](user/README.md)
    *   [Регистрация](user/sign-up.md)
    *   [Сброс пароля](user/password-reset.md)
*   [Синхронизация для работы в off-line](sync/README.md)
    *   [Введение](sync/intro.md)
    *   [Контекст данных](sync/context.md)
        *   [Публичные](sync/global.md)
        *   [Личные](sync/user.md)
        *   [Командные](sync/team.md)
    *   [API](sync/api.md)
*   On-line API для работы в реальном времени
    *   [Публичные данные](public/README.md)
    *   [Личные данные](private/README.md)
    *   [Командные данные](team/README.md)
    *   [Обратная связь](feedback/README.md)
*   [Связь с реальными банками](bank-integration/README.md)
    *   [Процедура установления связи](bank-integration/connect.md)
    *   [Варианты аутентификации](bank-integration/provider-modes.md)
    *   API
        *   [Логины](bank-integration/api-login.md)
*   [Типы данных](type/README.md)
*   [Прочее](misc/README.md)
    *   [Деление на страницы](misc/pagination.md)
    *   Статус коды ответов


[cubux.net]: https://www.cubux.net/
